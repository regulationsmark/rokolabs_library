USE [master]
GO

CREATE DATABASE [LibraryTaskDbo]
GO

USE [LibraryTaskDbo]
GO

CREATE TABLE [Authors](
	[Id] int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	[Name] nvarchar(50) NOT NULL,
	[Surname] nvarchar(200) NOT NULL,
	[IsDeleted] bit NOT NULL
)
GO

CREATE TABLE [BaseObjects](
	[Id] int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	[Name] nvarchar(300) NOT NULL,
	[PublicationYear] int NOT NULL,
	[CountOfPages] int NOT NULL,
	[Note] nvarchar(2000) NOT NULL,
	[IsDeleted] bit NOT NULL,
	[TypeLibraryObject] nvarchar(30) NOT NULL
)
GO

CREATE TABLE [LibraryObjects](
	[Id] int PRIMARY KEY NOT NULL,
	[Name] nvarchar(300) NOT NULL,
	[PublicationYear] int NOT NULL,
	[CountOfPages] int NOT NULL,
	[Note] nvarchar(2000) NOT NULL
)
GO

CREATE TABLE[Publications](
	[Id] int PRIMARY KEY NOT NULL,
	[PublishingHouse] nvarchar(300) NOT NULL,
	[PlaceOfPublication] nvarchar(200) NOT NULL
)
GO

CREATE TABLE [Books](
	[Id] int PRIMARY KEY NOT NULL,
	[ISBN] nvarchar(18) NULL
)
GO

CREATE TABLE [Newspapers](
	[Id] int PRIMARY KEY NOT NULL,
	[IssueNumber] int NULL,
	[IssueDate] datetime NOT NULL,
	[ISSN] nvarchar(14) NULL
)
GO

CREATE TABLE [Patents](
	[Id] int PRIMARY KEY NOT NULL,
	[Country] nvarchar(200) NOT NULL,
	[RegistrationNumber] int NOT NULL,
	[ApplicationDate] datetime NULL,
	[PublicationDate] datetime NOT NULL,
)
GO

CREATE TABLE [Roles] (
   [Id] int IDENTITY(1,1) PRIMARY KEY NOT NULL,
   [RoleName] nvarchar(50) NOT NULL,
   [IsDeleted] bit NOT NULL
	)
GO

CREATE TABLE [Users](
	[Id] int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	[Username] nvarchar(50) NOT NULL,
	[Password] varbinary(256) NULL,
	[IsDeleted] bit NOT NULL,
	[RoleId] int FOREIGN KEY REFERENCES [Roles](Id)
	ON UPDATE CASCADE 
    ON DELETE CASCADE
	)
GO

CREATE TABLE [BooksByAuthors](
	[IdBook] int FOREIGN KEY REFERENCES [Books](Id) NOT NULL,
	[IDAuthor] int FOREIGN KEY REFERENCES [Authors](Id)NOT NULL,
 )
GO
 CREATE TABLE [PatentsByInventors](
	[IdPatent] int FOREIGN KEY REFERENCES [Patents](Id)NOT NULL,
	[IDInventor] int FOREIGN KEY REFERENCES [Authors](Id) NOT NULL
 )
GO

ALTER TABLE [LibraryObjects] WITH CHECK ADD
 CONSTRAINT FK_LibraryObject_With_BaseObject FOREIGN KEY (Id) 
       REFERENCES [BaseObjects](Id);

ALTER TABLE [Publications] WITH CHECK ADD
 CONSTRAINT FK_Publication_With_BaseObject FOREIGN KEY (Id) 
       REFERENCES [BaseObjects](Id);
GO

ALTER TABLE [Books] WITH CHECK ADD
 CONSTRAINT FK_Book_With_BaseObject FOREIGN KEY (Id) 
       REFERENCES [BaseObjects](Id);
GO

ALTER TABLE [Newspapers] WITH CHECK ADD
 CONSTRAINT FK_Newspaper_With_BaseObject FOREIGN KEY (Id) 
       REFERENCES [BaseObjects](Id);
GO

ALTER TABLE [Patents] WITH CHECK ADD
 CONSTRAINT FK_Patent_With_BaseObject FOREIGN KEY (Id) 
       REFERENCES [BaseObjects](Id);
GO

CREATE PROCEDURE AddAuthor
@Name nvarchar(50),
@Surname nvarchar(200)
AS
BEGIN
INSERT INTO [Authors] ([Name], [Surname], [IsDeleted])
VALUES (@Name, @Surname, 0)
SELECT SCOPE_IDENTITY()
END
GO


CREATE PROCEDURE AddLibraryObject
@Id int,
@Name nvarchar(300),
@PublicationYear int,
@CountOfPages int,
@Note nvarchar(2000)
AS
BEGIN
INSERT INTO [LibraryObjects] ([Id], [Name], [PublicationYear], [CountOfPages], [Note])
VALUES (@Id, @Name, @PublicationYear, @CountOfPages, @Note)
SELECT SCOPE_IDENTITY()
END
GO

CREATE PROCEDURE AddPublication
@Id int,
@PublishingHouse nvarchar(300),
@PlaceOfPublication nvarchar(200)
AS
BEGIN
INSERT INTO [Publications] ([Id], [PublishingHouse], [PlaceOfPublication])
VALUES (@Id, @PublishingHouse, @PlaceOfPublication)
SELECT SCOPE_IDENTITY()
END
GO

CREATE PROCEDURE AddAuthorForBook
@BookId int,
@AuthorId int
AS
BEGIN
INSERT INTO [BooksByAuthors] ([IdBook], [IDAuthor])
VALUES (@BookId, @AuthorId)
END
GO

CREATE PROCEDURE AddInventorForPatent
@PatentId int,
@InventorId int
AS
BEGIN
INSERT INTO [PatentsByInventors] ([IdPatent], [IDInventor])
VALUES (@PatentId, @InventorId)
END
GO

CREATE TYPE [ListOfAuthors] AS TABLE(
	[AuthorId] int NULL
)
GO

CREATE PROCEDURE AddBook
@Name nvarchar(300),
@PublicationYear int,
@CountOfPages int,
@Note nvarchar(2000),
@PublishingHouse nvarchar(300),
@PlaceOfPublication nvarchar(200),
@ISBN nvarchar(18),
@AuthorsIds [dbo].[ListOfAuthors] READONLY
AS
BEGIN
INSERT INTO [BaseObjects] ([Name], [PublicationYear], [CountOfPages] , [Note], IsDeleted, [TypeLibraryObject])
	VALUES(@Name, @PublicationYear, @CountOfPages, @Note, 0, 'Book')
DECLARE @BaseObjectId INT
	SET @BaseObjectId = SCOPE_IDENTITY()
INSERT INTO [LibraryObjects]([Id], [Name], [PublicationYear], [CountOfPages], [Note])
	VALUES(@BaseObjectId, @Name, @PublicationYear, @CountOfPages, @Note)
INSERT INTO [Publications]([Id], [PublishingHouse], [PlaceOfPublication])
	VALUES(@BaseObjectId, @PublishingHouse, @PlaceOfPublication)
INSERT INTO [Books] ([Id], [ISBN])
	VALUES (@BaseObjectId, @ISBN)
INSERT INTO [BooksByAuthors]  ([IdBook], [IDAuthor])
	(SELECT @BaseObjectId, [AuthorId] FROM @AuthorsIds)
SELECT @BaseObjectId
END
GO

CREATE PROCEDURE AddNewspaper
@Name nvarchar(300),
@PublicationYear int,
@CountOfPages int,
@Note nvarchar(2000),
@PublishingHouse nvarchar(300),
@PlaceOfPublication nvarchar(200),
@IssueNumber int,
@IssueDate datetime,
@ISSN nvarchar(14)
AS
BEGIN
INSERT INTO [BaseObjects] ([Name], [PublicationYear], [CountOfPages] , [Note], IsDeleted, [TypeLibraryObject])
	VALUES(@Name, @PublicationYear, @CountOfPages, @Note, 0, 'Newspaper')
DECLARE @BaseObjectId INT
	SET @BaseObjectId = SCOPE_IDENTITY()
INSERT INTO [LibraryObjects]([Id], [Name], [PublicationYear], [CountOfPages], [Note])
	VALUES(@BaseObjectId, @Name, @PublicationYear, @CountOfPages, @Note)
INSERT INTO [Publications]([Id], [PublishingHouse], [PlaceOfPublication])
	VALUES(@BaseObjectId, @PublishingHouse, @PlaceOfPublication)
INSERT INTO [Newspapers] ([Id], [IssueNumber], [IssueDate], [ISSN])
	VALUES (@BaseObjectId, @IssueNumber , @IssueDate, @ISSN)
SELECT @BaseObjectId
END
GO

CREATE PROCEDURE AddPatent
@Name nvarchar(300),
@PublicationYear int,
@CountOfPages int,
@Note nvarchar(2000),
@Country nvarchar(200),
@RegistrationNumber int,
@ApplicationDate datetime,
@PublicationDate datetime,
@InventorsIds AS [ListOfAuthors] READONLY
AS
BEGIN
INSERT INTO [BaseObjects] ([Name], [PublicationYear], [CountOfPages] , [Note], IsDeleted, [TypeLibraryObject])
	VALUES(@Name, @PublicationYear, @CountOfPages, @Note, 0, 'Patent')
DECLARE @BaseObjectId INT
	SET @BaseObjectId = SCOPE_IDENTITY()
INSERT INTO [LibraryObjects]([Id], [Name], [PublicationYear], [CountOfPages], [Note])
	VALUES(@BaseObjectId, @Name, @PublicationYear, @CountOfPages, @Note)
INSERT INTO Patents ([Id], [Country], [RegistrationNumber], [ApplicationDate], [PublicationDate])
 VALUES(@BaseObjectId, @Country, @RegistrationNumber, @ApplicationDate, @PublicationDate)
INSERT INTO [PatentsByInventors]([IdPatent], [IDInventor])
	(SELECT @BaseObjectId, [AuthorId] FROM @InventorsIds)
SELECT @BaseObjectId
END
GO

CREATE PROCEDURE [AddRole]
@RoleName nvarchar(50)
AS
BEGIN
INSERT INTO [Roles](RoleName, IsDeleted)
VALUES(@RoleName, 0)
DECLARE @NewRoleId INT
SET @NewRoleId = SCOPE_IDENTITY()
SELECT @NewRoleId
END
GO

CREATE PROCEDURE [AddUser]
@Username nvarchar(50),
@Password varbinary(256)
AS
BEGIN
INSERT INTO [Users](Username, Password, IsDeleted, RoleId)
	VALUES(@Username,@Password, 0,
		(SELECT Id FROM [Roles] WHERE [RoleName] = 'NewUser' AND [IsDeleted] = 0))
DECLARE @NewUserId INT
SET @NewUserId = SCOPE_IDENTITY()
SELECT @NewUserId
END
GO

CREATE PROCEDURE DeleteAuthorById
@Id int
AS
BEGIN
UPDATE [Authors]
SET [IsDeleted] = 1
WHERE [Id] = @Id
END
GO

CREATE PROCEDURE DeleteBookById
@Id int
AS
BEGIN
UPDATE [BaseObjects]
SET [IsDeleted] = 1 WHERE [Id] = @Id AND [TypeLibraryObject] = 'Book'
DELETE FROM [Books] WHERE [Id] = @Id
DELETE FROM [LibraryObjects] WHERE [Id] = @Id
DELETE FROM [Publications] WHERE [Id] = @Id
END
GO

CREATE PROCEDURE DeleteOnlyLibraryObjectById
@Id int
AS
BEGIN
DELETE FROM [LibraryObjects] WHERE [Id] = @Id
END
GO

CREATE PROCEDURE DeletePublicationById
@Id int
AS
BEGIN
DELETE FROM [Publications] WHERE [Id] = @Id
END
GO

CREATE PROCEDURE DeleteNewspaperById
@Id int
AS
BEGIN
UPDATE [BaseObjects] 
SET [IsDeleted] = 1 WHERE [Id] = @Id AND [TypeLibraryObject] = 'Newspaper'
DELETE FROM [Newspapers]  WHERE [Id] = @Id
DELETE FROM [Publications]  WHERE [Id] = @Id
DELETE FROM [LibraryObjects]  WHERE [Id] = @Id
END
GO

CREATE PROCEDURE DeletePatentByID
@Id int
AS
BEGIN
UPDATE [BaseObjects]
SET [IsDeleted] = 1 WHERE [Id] = @Id AND [TypeLibraryObject] = 'Patent'
DELETE FROM [Patents]  WHERE [Id] = @Id
DELETE FROM [LibraryObjects]  WHERE [Id] = @Id
END
GO

CREATE PROCEDURE DeleteLibraryObjectById
@Id int
AS
BEGIN
DECLARE @TypeOfLibraryObject nvarchar(30)
SET @TypeOfLibraryObject = (SELECT [BaseObjects].[TypeLibraryObject] FROM [BaseObjects] WHERE [BaseObjects].[Id] = @Id)
IF @TypeOfLibraryObject = 'Book'
	EXEC DeleteBookById @Id
IF @TypeOfLibraryObject = 'Newspaper'
	EXEC DeleteNewspaperById @Id
IF @TypeOfLibraryObject = 'Patent'
	EXEC DeletePatentByID @Id
END
GO

CREATE PROCEDURE [DeleteUserById]
@Id INT
AS
BEGIN
UPDATE [Users]
SET [IsDeleted] = 1
WHERE [Id] = @Id
END
GO

CREATE PROCEDURE UpdateAuthor
@Id int,
@Name nvarchar(50),
@Surname nvarchar(200)
AS
BEGIN
UPDATE [Authors] 
SET [Name] = @Name, [Surname] = @Surname
WHERE [Id] = @Id AND [IsDeleted] = 0
END
GO

CREATE PROCEDURE UpdateLibraryObject
@Id int,
@Name nvarchar(300),
@PublicationYear int,
@CountOfPages int,
@Note nvarchar(2000)
AS
BEGIN
UPDATE [LibraryObjects] 
SET [Id] = @Id, [Name] = @Name, [PublicationYear] = @PublicationYear, [CountOfPages] = @CountOfPages, [Note] = @Note
WHERE [Id] = @Id
END
GO

CREATE PROCEDURE UpdatePublication
@Id int,
@PublishingHouse nvarchar(300),
@PlaceOfPublication nvarchar(200)
AS
BEGIN
UPDATE [Publications] 
SET [Id] = @Id, [PublishingHouse] = @PublishingHouse, [PlaceOfPublication] = @PlaceOfPublication
WHERE [Id] = @Id
END
GO

CREATE PROCEDURE UpdateBook
@Id int,
@Name nvarchar(300),
@PublicationYear int,
@CountOfPages int,
@Note nvarchar(2000),
@PublishingHouse nvarchar(300),
@PlaceOfPublication nvarchar(200),
@ISBN nvarchar(18),
@AuthorsIds AS [ListOfAuthors] READONLY
AS
BEGIN
UPDATE [BaseObjects]
	SET [Name] = @Name, [PublicationYear] = @PublicationYear, [CountOfPages] = @CountOfPages, [Note] = @Note
		WHERE [Id] = @Id AND [TypeLibraryObject] = 'Book' AND IsDeleted = 0
UPDATE [LibraryObjects]
	SET  [Name] = @Name, [PublicationYear] = @PublicationYear, [CountOfPages] = @CountOfPages, [Note] = @Note
		WHERE [Id] = @Id
UPDATE [Publications]
	SET [PublishingHouse] = @PublishingHouse, [PlaceOfPublication] = @PlaceOfPublication
		WHERE [Id] = @Id
UPDATE Books
	SET ISBN = @ISBN
		WHERE [Id] = @Id
UPDATE [BooksByAuthors]
	SET [BooksByAuthors].[IDAuthor] = (SELECT [AuthorId] FROM @AuthorsIds)
		WHERE [IdBook] = @Id
END
GO

CREATE PROCEDURE UpdateNewspaper
@Id int,
@Name nvarchar(300),
@PublicationYear int,
@CountOfPages int,
@Note nvarchar(2000),
@PublishingHouse nvarchar(300),
@PlaceOfPublication nvarchar(200),
@IssueNumber int,
@IssueDate datetime,
@ISSN nvarchar(14)
AS
BEGIN
UPDATE [BaseObjects] 
	SET [Name] = @Name, [PublicationYear] =  @PublicationYear, [CountOfPages] = @CountOfPages, [Note] = @Note
		WHERE [Id] = @Id AND [TypeLibraryObject] = 'Newspaper' AND [IsDeleted] = 0
UPDATE [LibraryObjects]
	SET  [Name] = @Name, [PublicationYear] = @PublicationYear, [CountOfPages] = @CountOfPages, [Note] = @Note
		WHERE [Id] = @Id
UPDATE [Publications]
	SET [PublishingHouse] = @PublishingHouse, [PlaceOfPublication] = @PlaceOfPublication
		WHERE [Id] = @Id
UPDATE [Newspapers]
	SET [IssueNumber] = @IssueNumber, [IssueDate] = @IssueDate, [ISSN] = @ISSN
WHERE [Id] = @Id
END
GO

CREATE PROCEDURE UpdatePatent
@Id int,
@Name nvarchar(300),
@PublicationYear int,
@CountOfPages int,
@Note nvarchar(2000),
@Country nvarchar(200),
@RegistrationNumber int,
@ApplicationDate datetime,
@PublicationDate datetime,
@InventorsIds AS [ListOfAuthors] READONLY
AS
BEGIN
UPDATE [BaseObjects]
SET 
	[Name] = @Name, 
	[PublicationYear] = @PublicationYear, 
	[CountOfPages] = @CountOfPages,
	[Note] = @Note
WHERE [Id] = @Id AND [TypeLibraryObject] = 'Patent' AND IsDeleted = 0
UPDATE [LibraryObjects]
	SET  [Name] = @Name, [PublicationYear] = @PublicationYear, [CountOfPages] = @CountOfPages, [Note] = @Note
		WHERE [Id] = @Id
UPDATE [Patents]
SET 
	[Country] = @Country, 
	[RegistrationNumber] = @RegistrationNumber, 
	[ApplicationDate] = @ApplicationDate,
	PublicationDate = @PublicationDate
WHERE [Id] = @Id
UPDATE [PatentsByInventors]
SET [PatentsByInventors].[IDInventor] = (SELECT [AuthorId] FROM @InventorsIds)
	WHERE [IdPatent] = @Id
END
GO

CREATE PROCEDURE [UpdateUserRoleWithRoleId]
@IdUser int,
@IdRole int
AS
BEGIN
UPDATE [Users]
SET [RoleId] = @IdRole
WHERE [Id] = @IdUser
END
GO

CREATE PROCEDURE [UpdateUserRoleWithRoleName]
@IdUser int,
@RoleName int
AS
BEGIN
UPDATE [Users]
SET [RoleId] = (SELECT Id FROM [Roles] WHERE [RoleName] = @RoleName AND [IsDeleted] = 0)
WHERE [Id] = @IdUser
END
GO

CREATE PROCEDURE GetAllAuthors
AS
BEGIN
SELECT [Id], [Name], [Surname]
FROM Authors WHERE IsDeleted = 0
END
GO

CREATE PROCEDURE GetAllBooks
AS
BEGIN
SELECT 
	[BaseObjects].[Id],
	[BaseObjects].[Name],
	[BaseObjects].[PublicationYear],
	[BaseObjects].[CountOfPages],
	[BaseObjects].[Note],
	[Publications].[PublishingHouse],
	[Publications].[PlaceOfPublication],
	[Books].[ISBN]
	FROM [BaseObjects]
	LEFT JOIN [Publications] ON [Publications].[Id] = [BaseObjects].[Id]
	LEFT JOIN [Books] ON [BaseObjects].[Id] = [Books].[Id]
	WHERE [BaseObjects].[IsDeleted] = 0 AND [BaseObjects].[TypeLibraryObject] = 'Book'

SELECT [BooksByAuthors].[IdAuthor], 
	[BooksByAuthors].[IdBook]
	FROM [BooksByAuthors]
	LEFT JOIN [BaseObjects] ON [BaseObjects].[Id] = [BooksByAuthors].[IdBook]
		WHERE [BaseObjects].[IsDeleted] = 0

DECLARE @books TABLE (bookId int)
INSERT INTO @books
	SELECT [BaseObjects].[Id]
	FROM [BaseObjects]
	LEFT JOIN [Books] ON [BaseObjects].[Id] = [Books].[Id]
	WHERE [BaseObjects].[IsDeleted] = 0 AND [BaseObjects].[TypeLibraryObject] = 'Book'

DECLARE @authors TABLE (authorId int)
INSERT INTO @authors
	SELECT [BooksByAuthors].[IdAuthor]
	FROM [BooksByAuthors]
	WHERE [IdBook] in (SELECT [bookId] FROM @books)

SELECT 
	[Id] AS authorId,
	[Name] AS authorName,
	[Surname] AS authorSurname
		FROM [Authors]
		WHERE [Id] IN (SELECT [authorId] FROM @authors)
END
GO

CREATE PROCEDURE GetAllNewspapers
AS
BEGIN
SELECT 
	[BaseObjects].[Id],
	[BaseObjects].[Name],
	[BaseObjects].[PublicationYear],
	[BaseObjects].[CountOfPages],
	[BaseObjects].[Note],
	[Newspapers].[IssueDate],
	[Newspapers].[IssueNumber],
	[Publications].[PublishingHouse],
	[Publications].[PlaceOfPublication],
	[Newspapers].[ISSN]
FROM [BaseObjects]
INNER JOIN [Publications] ON [Publications].[Id] = [BaseObjects].[Id]
INNER JOIN [Newspapers] ON [BaseObjects].[Id] = [Newspapers].[Id]
WHERE [BaseObjects].[IsDeleted] = 0 AND [BaseObjects].[TypeLibraryObject] = 'Newspaper'
END
GO

CREATE PROCEDURE GetAllPatents
AS
BEGIN
SELECT
	[BaseObjects].[Id],
	[BaseObjects].[Name],
	[BaseObjects].[PublicationYear],
	[BaseObjects].[CountOfPages],
	[BaseObjects].[Note],
	[Patents].[Country],
	[Patents].[RegistrationNumber],
	[Patents].[ApplicationDate],
	[Patents].[PublicationDate]
FROM [BaseObjects] 
INNER JOIN [Patents] ON [BaseObjects].[Id] = [Patents].[Id]
WHERE [BaseObjects].[IsDeleted] = 0 AND [BaseObjects].[TypeLibraryObject] = 'Patent'

SELECT [PatentsByInventors].[IDInventor], 
	[PatentsByInventors].[IdPatent]
	FROM [PatentsByInventors]
	LEFT JOIN [BaseObjects] ON [BaseObjects].[Id] = [PatentsByInventors].[IdPatent]
		WHERE [BaseObjects].[IsDeleted] = 0

DECLARE @patents TABLE (patentId int)
INSERT INTO @patents
	SELECT [BaseObjects].[Id]
	FROM [BaseObjects]
	LEFT JOIN [Patents] ON [BaseObjects].[Id] = [Patents].[Id]
	WHERE [BaseObjects].[IsDeleted] = 0 AND [BaseObjects].[TypeLibraryObject] = 'Patent'

DECLARE @inventors TABLE (inventorId int)
INSERT INTO @inventors
	SELECT [PatentsByInventors].[IDInventor]
	FROM [PatentsByInventors]
	WHERE [IdPatent] in (SELECT patentId FROM @patents)

SELECT 
	[Id] AS authorId,
	[Name] AS authorName,
	[Surname] AS authorSurname
		FROM [Authors]
		WHERE [Id] IN (SELECT inventorId FROM @inventors)

END
GO

CREATE PROCEDURE GetTypeOfLibraryObject
AS
BEGIN
SELECT
	[BaseObjects].[TypeLibraryObject]
	FROM [BaseObjects]
END
GO

CREATE PROCEDURE GetAllLibraryObjects
AS
BEGIN
SELECT 
	[BaseObjects].[Id],
	[BaseObjects].[Name],
	[BaseObjects].[PublicationYear],
	[BaseObjects].[CountOfPages],
	[BaseObjects].[Note],
	[BaseObjects].[TypeLibraryObject],
	[Books].[ISBN],
	[Newspapers].[IssueDate],
	[Newspapers].[IssueNumber],
	[Newspapers].[ISSN],
	[Patents].[Country],
	[Patents].[RegistrationNumber],
	[Patents].[ApplicationDate],
	[Patents].[PublicationDate]
FROM [BaseObjects]
LEFT JOIN [Books] ON [Books].[Id] = [BaseObjects].[Id]
LEFT JOIN [Newspapers] ON [Newspapers].[Id] = [BaseObjects].[Id]
LEFT JOIN [Patents] ON [Patents].[Id] = [BaseObjects].[Id]
WHERE [BaseObjects].[IsDeleted] = 0
END
GO

CREATE PROCEDURE GetLibraryObjectsBySearchingString
@SearchingString nvarchar(50)
AS
BEGIN
DECLARE @libobjects TABLE (objectId int)

	INSERT INTO @libobjects
	SELECT [BaseObjects].[Id]
	FROM [BaseObjects]
		WHERE [BaseObjects].[Name] like '%'+@SearchingString+'%'

DECLARE @authors TABLE (authorId int)

	INSERT INTO @authors
	SELECT [BooksByAuthors].[IDAuthor]
		FROM [BooksByAuthors]
		WHERE [BooksByAuthors].[IdBook] in (SELECT objectId FROM @libobjects)

DECLARE @inventors TABLE (inventorId int)

	INSERT INTO @inventors
	SELECT [PatentsByInventors].[IDInventor]
		FROM [PatentsByInventors]
		WHERE [PatentsByInventors].[IdPatent] in (SELECT objectId FROM @libobjects)

SELECT 
	[BaseObjects].[Id],
	[BaseObjects].[Name],
	[BaseObjects].[PublicationYear],
	[BaseObjects].[CountOfPages],
	[BaseObjects].[Note],
	[BaseObjects].[TypeLibraryObject],
	[Publications].[PublishingHouse],
	[Publications].[PlaceOfPublication],
	[Books].[ISBN],
	[Newspapers].[IssueDate],
	[Newspapers].[IssueNumber],
	[Newspapers].[ISSN],
	[Patents].[Country],
	[Patents].[RegistrationNumber],
	[Patents].[ApplicationDate],
	[Patents].[PublicationDate]
FROM [BaseObjects]
INNER JOIN [Publications] ON [BaseObjects].[Id] = [Publications].[Id]
INNER JOIN [Books] ON [BaseObjects].[Id] = [Books].[Id]
INNER JOIN [Newspapers] ON [BaseObjects].[Id] = [Newspapers].[Id]
INNER JOIN [Patents] ON [BaseObjects].[Id] = [Patents].[Id]
		WHERE [BaseObjects].[Id] IN (SELECT objectId FROM @libobjects)

SELECT [BooksByAuthors].[IDAuthor], [BooksByAuthors].[IdBook] 
		FROM [BooksByAuthors]
		WHERE [BooksByAuthors].[IdBook]  IN (SELECT objectId FROM @libobjects)

SELECT [PatentsByInventors].[IDInventor], [PatentsByInventors].[IdPatent]
		FROM [PatentsByInventors]
		WHERE [PatentsByInventors].[IdPatent]  IN (SELECT objectId FROM @libobjects)

SELECT 
	[Id] AS authorId,
	[Name] AS authorName,
	[Surname] AS authorSurname
		FROM [Authors]
		WHERE [Id] IN (SELECT authorId FROM @authors);

SELECT 
	[Id] AS authorId,
	[Name] AS authorName,
	[Surname] AS authorSurname
		FROM [Authors]
		WHERE [Id] IN (SELECT inventorId FROM @inventors);

END
GO

CREATE PROCEDURE [GetAllRoles]
AS
BEGIN
SELECT [Id], [RoleName]
FROM [Roles]
END
GO

CREATE PROCEDURE [GetAllUsers]
AS
BEGIN
SELECT 
[Users].[Id], [Users].[Username]
FROM [Users]
LEFT JOIN [Roles] ON [Roles].[Id] = [Users].[RoleId] 
WHERE [Users].[IsDeleted] = 0
END
GO

CREATE PROCEDURE GetAuthorById
@Id int
AS
BEGIN
SELECT 
	[Authors].[Id],
	[Authors].[Name],
	[Authors].[Surname]
FROM [Authors] 
WHERE [Authors].Id = @Id AND [Authors].[IsDeleted] = 0
END
GO

CREATE PROCEDURE GetLibraryObjectById
@Id int
AS
BEGIN
SELECT 
	[LibraryObjects].[Id],
	[LibraryObjects].[Name],
	[LibraryObjects].[PublicationYear],
	[LibraryObjects].[CountOfPages],
	[LibraryObjects].[Note]
FROM [LibraryObjects] 
WHERE [LibraryObjects].Id = @Id
END
GO

CREATE PROCEDURE GetNewpaperById
@Id int
AS
BEGIN
SELECT 
	[BaseObjects].[Id],
	[BaseObjects].[Name],
	[BaseObjects].[PublicationYear],
	[BaseObjects].[CountOfPages],
	[BaseObjects].[Note],
	[Newspapers].[IssueDate],
	[Newspapers].[IssueNumber],
	[Publications].[PublishingHouse],
	[Publications].[PlaceOfPublication],
	[Newspapers].[ISSN]
FROM [BaseObjects]
INNER JOIN [Publications] ON [Publications].[Id] = [BaseObjects].[Id]
INNER JOIN [Newspapers] ON [BaseObjects].[Id] = [Newspapers].[Id]
WHERE [BaseObjects].[Id] = @Id AND [BaseObjects].[IsDeleted] = 0 AND [BaseObjects].[TypeLibraryObject] = 'Newspaper'
END
GO

CREATE PROCEDURE GetPublicationById
@Id int
AS
BEGIN
SELECT 
	[BaseObjects].[Id],
	[BaseObjects].[Name],
	[BaseObjects].[PublicationYear],
	[BaseObjects].[CountOfPages],
	[BaseObjects].[Note],
	[BaseObjects].[TypeLibraryObject],
	[Publications].[PublishingHouse],
	[Publications].[PlaceOfPublication]
FROM [Publications]
INNER JOIN  [BaseObjects] ON [BaseObjects].[Id] = [Publications].[Id]
WHERE [Publications].Id = @Id
END
GO

CREATE PROCEDURE GetAuthorsByBookId
@Id int
AS
BEGIN
SELECT 
	[Authors].[Id],
	[Authors].[Name],
	[Authors].[Surname]
FROM [Authors]
INNER JOIN [BooksByAuthors] ON [BooksByAuthors].[IDAuthor] = [Authors].[Id]
INNER JOIN [Books] ON [Books].[Id] = [BooksByAuthors].[IdBook]
WHERE [BooksByAuthors].[IdBook] = @Id
END
GO

CREATE PROCEDURE GetInventorsByPatentId
@Id int
AS
BEGIN
SELECT
	[Authors].[Id],
	[Authors].[Name],
	[Authors].[Surname]
FROM [Authors]
INNER JOIN [PatentsByInventors] ON [PatentsByInventors].[IDInventor] = [Authors].[Id]
INNER JOIN [Patents] ON [Patents].[Id] = [PatentsByInventors].[IdPatent]
WHERE [PatentsByInventors].[IdPatent] = @Id 
END
GO

CREATE PROCEDURE GetBookById
@Id int
AS
BEGIN

DECLARE @authors TABLE (authorId int)

	INSERT INTO @authors
	SELECT [BooksByAuthors].[IDAuthor]
		FROM [BooksByAuthors]
		WHERE [IdBook] = @Id


SELECT 
	[BaseObjects].[Id],
	[BaseObjects].[Name],
	[BaseObjects].[PublicationYear],
	[BaseObjects].[CountOfPages],
	[BaseObjects].[Note],
	[Publications].[PublishingHouse],
	[Publications].[PlaceOfPublication],
	[Books].[ISBN]
FROM [BaseObjects]
INNER JOIN [Publications] ON [BaseObjects].[Id] = Publications.[Id]
INNER JOIN [Books] ON [BaseObjects].[Id] = [Books].[Id]
WHERE [BaseObjects].[Id] = @Id AND [BaseObjects].[IsDeleted] = 0  AND [BaseObjects].[TypeLibraryObject] = 'Book'

SELECT [BooksByAuthors].[IdAuthor], 
	[BooksByAuthors].[IdBook]
	FROM [BooksByAuthors]
		WHERE [BooksByAuthors].[IdBook] = @Id

SELECT 
	[Id] AS authorId,
	[Name] AS authorName,
	[Surname] AS authorSurname
		FROM [Authors]
		WHERE [Id] IN (SELECT [authorId] FROM @authors)

END
GO

CREATE PROCEDURE GetBooksByAuthorId
@Id int
AS
BEGIN

DECLARE @books TABLE (bookId int)
INSERT INTO @books
	SELECT [BooksByAuthors].[IdBook]
	FROM [BooksByAuthors]
	WHERE [BooksByAuthors].[IDAuthor] = @Id

SELECT 
	[BaseObjects].[Id],
	[BaseObjects].[Name],
	[BaseObjects].[PublicationYear],
	[BaseObjects].[CountOfPages],
	[BaseObjects].[Note],
	[Publications].[PublishingHouse],
	[Publications].[PlaceOfPublication],
	[Books].[ISBN]
FROM [BaseObjects]
INNER JOIN [Books] ON [BaseObjects].[Id] = [Books].[Id]
INNER JOIN [Publications] ON [BaseObjects].[Id] = [Publications].[Id]
WHERE [BaseObjects].[TypeLibraryObject] = 'Book' AND [BaseObjects].[IsDeleted] = 0 AND [BaseObjects].[Id] IN (SELECT bookId FROM @books)

SELECT 
	[Id] AS authorId,
	[Name] AS authorName,
	[Surname] AS authorSurname
		FROM [Authors]
		WHERE [Id] = @Id

END
GO

CREATE PROCEDURE GetNewspaperById
@Id int
AS
BEGIN
SELECT 
	[BaseObjects].[Id],
	[BaseObjects].[Name],
	[BaseObjects].[PublicationYear],
	[BaseObjects].[CountOfPages],
	[BaseObjects].[Note],
	[Publications].[PublishingHouse],
	[Publications].[PlaceOfPublication],
	[Newspapers].[IssueDate],
	[Newspapers].[IssueNumber],
	[Newspapers].[ISSN]
FROM [BaseObjects] 
INNER JOIN [Publications] ON [BaseObjects].[Id] = [Publications].[Id]
INNER JOIN [Newspapers] ON [BaseObjects].[Id] = [Newspapers].[Id]
WHERE [BaseObjects].[Id] = @Id AND [BaseObjects].[IsDeleted] = 0 AND [BaseObjects].[TypeLibraryObject] = 'Newspaper'
END
GO

CREATE PROCEDURE GetPatentById
@Id int
AS
BEGIN

DECLARE @authors TABLE (authorId int)

	INSERT INTO @authors
	SELECT [PatentsByInventors].[IDInventor]
		FROM [PatentsByInventors]
		WHERE [IdPatent] = @Id


SELECT 
	[BaseObjects].[Id],
	[BaseObjects].[Name],
	[BaseObjects].[PublicationYear],
	[BaseObjects].[CountOfPages],
	[BaseObjects].[Note],
	[Patents].[Country],
	[Patents].[RegistrationNumber],
	[Patents].[ApplicationDate],
	[Patents].[PublicationDate]
FROM [BaseObjects] 
INNER JOIN [Patents] ON [BaseObjects].Id = [Patents].[Id]
WHERE [BaseObjects].[Id] = @Id AND [BaseObjects].[IsDeleted] = 0  AND [BaseObjects].[TypeLibraryObject] = 'Patent'

SELECT [PatentsByInventors].[IDInventor], 
	[PatentsByInventors].[IdPatent]
	FROM [PatentsByInventors]
		WHERE [PatentsByInventors].[IdPatent] = @Id

SELECT 
	[Id] AS authorId,
	[Name] AS authorName,
	[Surname] AS authorSurname
		FROM [Authors]
		WHERE [Id] IN (SELECT [authorId] FROM @authors)
END
GO

CREATE PROCEDURE GetPatentsByInventorId
@Id int
AS
BEGIN

DECLARE @patents TABLE (patentId int)
INSERT INTO @patents
	SELECT [PatentsByInventors].[IdPatent]
	FROM [PatentsByInventors]
	WHERE [PatentsByInventors].[IDInventor] = @Id

SELECT 
	[BaseObjects].[Id],
	[BaseObjects].[Name],
	[BaseObjects].[PublicationYear],
	[BaseObjects].[CountOfPages],
	[BaseObjects].[Note],
	[Patents].[Country],
	[Patents].[RegistrationNumber],
	[Patents].[ApplicationDate],
	[Patents].[PublicationDate]
FROM [BaseObjects]
INNER JOIN [Patents] ON [Patents].[Id] = [BaseObjects].[Id]
WHERE [BaseObjects].[TypeLibraryObject] = 'Patent' AND [BaseObjects].[IsDeleted] = 0 AND [BaseObjects].[Id] IN (SELECT patentId FROM @patents)

SELECT 
	[Id] AS authorId,
	[Name] AS authorName,
	[Surname] AS authorSurname
		FROM [Authors]
		WHERE [Id] = @Id
END
GO

CREATE PROCEDURE GetPatentsAndBooksByAuthorId
@Id int
AS
BEGIN

DECLARE @patents TABLE (patentId int)
INSERT INTO @patents
	SELECT [PatentsByInventors].[IdPatent]
	FROM [PatentsByInventors]
	WHERE [PatentsByInventors].[IDInventor] = @Id

DECLARE @books TABLE (bookId int)
INSERT INTO @books
	SELECT [BooksByAuthors].[IdBook]
	FROM [BooksByAuthors]
	WHERE [BooksByAuthors].[IDAuthor] = @Id

SELECT 
	[BaseObjects].[Id],
	[BaseObjects].[Name],
	[BaseObjects].[PublicationYear],
	[BaseObjects].[CountOfPages],
	[BaseObjects].[Note],
	[BaseObjects].[TypeLibraryObject],
	[Publications].[PublishingHouse],
	[Publications].[PlaceOfPublication],
	[Books].[ISBN],
	[Patents].[Country],
	[Patents].[RegistrationNumber],
	[Patents].[ApplicationDate],
	[Patents].[PublicationDate]
FROM [BaseObjects] 
LEFT JOIN [Publications] ON [BaseObjects].[Id] = Publications.[Id]
LEFT JOIN [Books] ON [BaseObjects].[Id] = [Books].[Id]
LEFT JOIN [Patents] ON [BaseObjects].[Id] = [Patents].[Id]
WHERE ([BaseObjects].[TypeLibraryObject] = 'Patent' OR [BaseObjects].[TypeLibraryObject] = 'Book') AND [BaseObjects].[IsDeleted] = 0
AND ([BaseObjects].[Id] IN (SELECT patentId FROM @patents) OR [BaseObjects].[Id] IN (SELECT bookId FROM @books))

SELECT 
	[Id] AS authorId,
	[Name] AS authorName,
	[Surname] AS authorSurname
		FROM [Authors]
		WHERE [Id] = @Id
END
GO

CREATE PROCEDURE GetSortedBookByPublicationYear
AS
BEGIN

DECLARE @libobjects TABLE (objectId int)

	INSERT INTO @libobjects
	SELECT [BaseObjects].[Id]
		FROM [BaseObjects]
		WHERE [BaseObjects].[IsDeleted] = 0

DECLARE @authors TABLE (authorId int)

	INSERT INTO @authors
	SELECT [BooksByAuthors].[IDAuthor]
		FROM [BooksByAuthors]
		WHERE [BooksByAuthors].[IdBook] in (SELECT objectId FROM @libobjects)

DECLARE @inventors TABLE (inventorId int)

	INSERT INTO @inventors
	SELECT [PatentsByInventors].[IDInventor]
		FROM [PatentsByInventors]
		WHERE [PatentsByInventors].[IdPatent] in (SELECT objectId FROM @libobjects)

SELECT 
	[BaseObjects].[Id],
	[BaseObjects].[Name],
	[BaseObjects].[PublicationYear],
	[BaseObjects].[CountOfPages],
	[BaseObjects].[Note],
	[BaseObjects].[TypeLibraryObject],
	[Publications].[PublishingHouse],
	[Publications].[PlaceOfPublication],
	[Books].[ISBN],
	[Newspapers].[IssueDate],
	[Newspapers].[IssueNumber],
	[Newspapers].[ISSN],
	[Patents].[Country],
	[Patents].[RegistrationNumber],
	[Patents].[ApplicationDate],
	[Patents].[PublicationDate]
FROM [BaseObjects] 
LEFT JOIN [Publications] ON [BaseObjects].[Id] = [Publications].[Id]
LEFT JOIN [Books] ON [Books].[Id] = [BaseObjects].[Id]
LEFT JOIN [Newspapers] ON [Newspapers].[Id] = [BaseObjects].[Id]
lEFT JOIN [Patents] ON [Patents].[Id] = [BaseObjects].[Id]
WHERE [BaseObjects].[IsDeleted] = 0
ORDER BY [BaseObjects].[PublicationYear] 

SELECT [BooksByAuthors].[IDAuthor], [BooksByAuthors].[IdBook] 
		FROM [BooksByAuthors]
		WHERE [BooksByAuthors].[IdBook] IN (SELECT objectId FROM @libobjects)

SELECT [PatentsByInventors].[IDInventor], [PatentsByInventors].[IdPatent]
		FROM [PatentsByInventors]
		WHERE [PatentsByInventors].[IdPatent]  IN (SELECT objectId FROM @libobjects)

SELECT 
	[Id] AS authorId,
	[Name] AS authorName,
	[Surname] AS authorSurname
		FROM [Authors]
		WHERE [Id] IN (SELECT authorId FROM @authors);

SELECT 
	[Id] AS authorId,
	[Name] AS authorName,
	[Surname] AS authorSurname
		FROM [Authors]
		WHERE [Id] IN (SELECT inventorId FROM @inventors);
END
GO

CREATE PROCEDURE GetReverseSortedBookByPublicationYear
AS
BEGIN
DECLARE @libobjects TABLE (objectId int)

	INSERT INTO @libobjects
	SELECT [BaseObjects].[Id]
		FROM [BaseObjects]
		WHERE [BaseObjects].[IsDeleted] = 0

DECLARE @authors TABLE (authorId int)

	INSERT INTO @authors
	SELECT [BooksByAuthors].[IDAuthor]
		FROM [BooksByAuthors]
		WHERE [BooksByAuthors].[IdBook] in (SELECT objectId FROM @libobjects)

DECLARE @inventors TABLE (inventorId int)

	INSERT INTO @inventors
	SELECT [PatentsByInventors].[IDInventor]
		FROM [PatentsByInventors]
		WHERE [PatentsByInventors].[IdPatent] in (SELECT objectId FROM @libobjects)

SELECT 
	[BaseObjects].[Id],
	[BaseObjects].[Name],
	[BaseObjects].[PublicationYear],
	[BaseObjects].[CountOfPages],
	[BaseObjects].[Note],
	[BaseObjects].[TypeLibraryObject],
	[Publications].[PublishingHouse],
	[Publications].[PlaceOfPublication],
	[Books].[ISBN],
	[Newspapers].[IssueDate],
	[Newspapers].[IssueNumber],
	[Newspapers].[ISSN],
	[Patents].[Country],
	[Patents].[RegistrationNumber],
	[Patents].[ApplicationDate],
	[Patents].[PublicationDate]
FROM [BaseObjects] 
LEFT JOIN [Publications] ON [BaseObjects].[Id] = [Publications].[Id]
LEFT JOIN [Books] ON [Books].[Id] = [BaseObjects].[Id]
LEFT JOIN [Newspapers] ON [Newspapers].[Id] = [BaseObjects].[Id]
lEFT JOIN [Patents] ON [Patents].[Id] = [BaseObjects].[Id]
WHERE [BaseObjects].[IsDeleted] = 0
ORDER BY [BaseObjects].[PublicationYear] DESC

SELECT [BooksByAuthors].[IDAuthor], [BooksByAuthors].[IdBook] 
		FROM [BooksByAuthors]
		WHERE [BooksByAuthors].[IdBook] IN (SELECT objectId FROM @libobjects)

SELECT [PatentsByInventors].[IDInventor], [PatentsByInventors].[IdPatent]
		FROM [PatentsByInventors]
		WHERE [PatentsByInventors].[IdPatent]  IN (SELECT objectId FROM @libobjects)

SELECT 
	[Id] AS authorId,
	[Name] AS authorName,
	[Surname] AS authorSurname
		FROM [Authors]
		WHERE [Id] IN (SELECT authorId FROM @authors);

SELECT 
	[Id] AS authorId,
	[Name] AS authorName,
	[Surname] AS authorSurname
		FROM [Authors]
		WHERE [Id] IN (SELECT inventorId FROM @inventors);
END
GO

CREATE PROCEDURE GetBooksByCharacterSetPublishingHouse
@CharacterSetPublishingHouse nvarchar(300)
AS
BEGIN
DECLARE @books TABLE (bookId int)

	INSERT INTO @books
	SELECT [BaseObjects].[Id]
	FROM [BaseObjects]
		WHERE [BaseObjects].[Name] like '%'+@CharacterSetPublishingHouse+'%' AND [BaseObjects].[TypeLibraryObject] = 'Book'

DECLARE @authors TABLE (authorId int)

	INSERT INTO @authors
	SELECT [BooksByAuthors].[IDAuthor]
		FROM [BooksByAuthors]
		WHERE [BooksByAuthors].[IdBook] in (SELECT bookId FROM @books)

SELECT 
	[BaseObjects].[Id],
	[BaseObjects].[Name],
	[BaseObjects].[PublicationYear],
	[BaseObjects].[CountOfPages],
	[BaseObjects].[Note],
    [Books].[ISBN],
	[Publications].[PublishingHouse],
	[Publications].[PlaceOfPublication]
	FROM [Books]
	INNER JOIN [BaseObjects] ON [Books].[Id] = [BaseObjects].[Id]
	INNER JOIN [Publications] ON [Books].[Id] = [Publications].[Id]
		WHERE [BaseObjects].[Id] IN (SELECT bookId FROM @books)

SELECT [BooksByAuthors].[IDAuthor], [BooksByAuthors].[IdBook] 
		FROM [BooksByAuthors]
		WHERE [BooksByAuthors].[IdBook]  IN (SELECT bookId FROM @books)

SELECT 
	[Id] AS authorId,
	[Name] AS authorName,
	[Surname] AS authorSurname
		FROM [Authors]
		WHERE [Id] IN (SELECT authorId FROM @authors);

END
GO

CREATE PROCEDURE GetAuthorByName
@PartName nvarchar(50)
AS
SELECT @PartName = RTRIM(@PartName) + '%' 
BEGIN
SELECT 
	[Authors].[Id],
	[Authors].[Name],
	[Authors].[Surname]
FROM [Authors]
	WHERE [Name] LIKE @PartName OR [Surname] LIKE @PartName AND IsDeleted=0
END
GO

CREATE PROCEDURE GetLibraryObjectByName
@PartName nvarchar(300)
AS
SELECT @PartName = RTRIM(@PartName) + '%' 
BEGIN
SELECT 
	[LibraryObjects].[Id],
	[LibraryObjects].[Name],
	[LibraryObjects].[PublicationYear],
	[LibraryObjects].[CountOfPages],
	[LibraryObjects].[Note]
FROM [LibraryObjects]
	WHERE [Name] LIKE @PartName
END
GO

CREATE PROCEDURE GetAuthorsByBookId
@Id int
AS
BEGIN

DECLARE @authors TABLE (authorId int)
INSERT INTO @authors
	SELECT [BooksByAuthors].[IDAuthor]
	FROM [BooksByAuthors]
	WHERE [BooksByAuthors].[IdBook] = @Id

SELECT 
	[Authors].[Id],
	[Authors].[Name],
	[Authors].[Surname]
FROM [Authors]
WHERE [Authors].[IsDeleted] = 0 AND [Authors].[Id] IN (SELECT authorId FROM @authors)
END
GO

CREATE PROCEDURE GetInventorsByPatentId
@Id int
AS
BEGIN

DECLARE @inventors TABLE (inventorId int)
INSERT INTO @inventors
	SELECT [PatentsByInventors].[IDInventor]
	FROM [PatentsByInventors]
	WHERE [PatentsByInventors].[IdPatent] = @Id

SELECT 
	[Authors].[Id],
	[Authors].[Name],
	[Authors].[Surname]
FROM [Authors]
WHERE [Authors].[IsDeleted] = 0 AND [Authors].[Id] IN (SELECT inventorId FROM @inventors)
END
GO

CREATE PROCEDURE [Login]
@Username  nvarchar(50),
@Password nvarchar(256)
AS
BEGIN
SELECT COUNT(*) FROM [Users] WHERE Username=@Username AND Password=@Password AND IsDeleted =0
END
GO
