﻿using Davydova_Task4_Library.BLL.Interfaces;
using Davydova_Task4_Library.Library.Documents;
using Davydova_Task4_Library.Library.OtherEntities;
using Davydova_Task4_Library.Library.Publications;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.PL
{
    public class Worker: BackgroundService
    {
        IAuthorLogic _authorLogic;
        IBookLogic _bookLogic;
        ILibraryObjectLogic _libraryObjectLogic;
        INewspaperLogic _newspaperLogic;
        IPatientLogic _patientLogic;

        public Worker(IAuthorLogic authorLogic, IBookLogic bookLogic, ILibraryObjectLogic libraryObjectLogic, INewspaperLogic newspaperLogic, IPatientLogic patientLogic)
        {
            _authorLogic = authorLogic;
            _bookLogic = bookLogic;
            _libraryObjectLogic = libraryObjectLogic;
            _newspaperLogic = newspaperLogic;
            _patientLogic = patientLogic;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            ConsoleIO.PrintMenu();//cycling menu
            while (true)
            {
                var action = ConsoleIO.EnterAction();
                switch (action)
                {
                    case "0":
                        int idAuthor = ConsoleIO.EnterObjectId();
                        string[] nameAndSurname = new string[2];
                        nameAndSurname = ConsoleIO.EnterAuthorInformation();
                        ConsoleIO.PrintInformation($"Author with id {_authorLogic.AddAuthor(idAuthor, nameAndSurname[0], nameAndSurname[1])} was added.");
                        break;
                    case "1":
                        int idBook = ConsoleIO.EnterObjectId();
                        string nameBook = ConsoleIO.EnterObjectName();
                        string bookCountOfPages = ConsoleIO.EnterCountOfPages();
                        string bookNote = ConsoleIO.EnterNote();
                        string bookPlaceOfPublication = ConsoleIO.EnterPlaceOfPublication();
                        string bookPublishingHouse = ConsoleIO.EnterPublishingHouse();
                        string bookPublishingYear = ConsoleIO.EnterPublishingYear();
                        string bookISBN = ConsoleIO.EnterBookISBN();
                        
                        List<string> stringsAuthors = new List<string>();                       
                        stringsAuthors = ConsoleIO.EnterAuthors();

                        List<Author> authorsList = new List<Author>();
                        foreach (string itemAuthor in stringsAuthors)
                        {
                            authorsList.Add(_authorLogic.GetAuthorById(int.Parse(itemAuthor)));
                        }

                        ConsoleIO.PrintInformation($"Book with id {_bookLogic.AddBook(idBook, nameBook, bookCountOfPages, bookNote, bookPlaceOfPublication, bookPublishingHouse, bookPublishingYear, authorsList, bookISBN)} was added.");
                        int bookYear;
                        bool bookSuccess = Int32.TryParse(bookPublishingYear, out bookYear);
                        if (bookSuccess)
                        {
                            _libraryObjectLogic.AddLibraryObject(idBook, nameBook, bookYear, bookCountOfPages, bookNote);
                        }
                        break;
                    case "2":
                        int idNewspaper = ConsoleIO.EnterObjectId();
                        string nameNewspaper = ConsoleIO.EnterObjectName();
                        string newspaperPublishingYear = ConsoleIO.EnterPublishingYear();
                        string newspaperCountOfPages = ConsoleIO.EnterCountOfPages();
                        string newspaperNote = ConsoleIO.EnterNote();
                        string newspaperPlaceOfPublication = ConsoleIO.EnterPlaceOfPublication();
                        string newspaperPublishingHouse = ConsoleIO.EnterPublishingHouse();
                        int newspaperIssueNumber = ConsoleIO.EnterIssueNumber();
                        DateTime newspaperIssueDate = ConsoleIO.EnterIssueDate();
                        string newspaperISSN = ConsoleIO.EnterISSNNewspaper();

                        ConsoleIO.PrintInformation($"Newspaper with id {_newspaperLogic.AddNewspaper(idNewspaper, nameNewspaper, newspaperPublishingYear, newspaperCountOfPages, newspaperNote, newspaperPlaceOfPublication, newspaperPublishingHouse, newspaperIssueNumber, newspaperIssueDate, newspaperISSN)} was added.");
                        int newspaperYear;
                        bool newspaperSuccess = Int32.TryParse(newspaperPublishingYear, out newspaperYear);
                        if (newspaperSuccess)
                        {
                            _libraryObjectLogic.AddLibraryObject(idNewspaper, nameNewspaper, newspaperYear, newspaperCountOfPages, newspaperNote);
                        }
                        break;
                    case "3":
                        int idPatent = ConsoleIO.EnterObjectId();
                        string namePatent = ConsoleIO.EnterObjectName();
                        string patentCountOfPages = ConsoleIO.EnterCountOfPages();
                        string patentNote = ConsoleIO.EnterNote();
                        string patentRegistrationNumber = ConsoleIO.EnterPatentRegistrationNumber();
                        DateTime patentApplicationDate = ConsoleIO.EnterApplicationDate();
                        DateTime patentPublicationDate = ConsoleIO.EnterPublicationDate();
                        string patentCountry = ConsoleIO.EnterCountry();

                        List<string> stringsInventors = new List<string>();
                        stringsInventors = ConsoleIO.EnterAuthors();

                        List<Author> inventorsList = new List<Author>();
                        foreach (string itemInventor in stringsInventors)
                        {
                            inventorsList.Add(_authorLogic.GetAuthorById(int.Parse(itemInventor)));
                        }

                        ConsoleIO.PrintInformation($"Patent with id {_patientLogic.AddPatent(idPatent, namePatent, patentCountOfPages, patentNote, inventorsList, patentRegistrationNumber, patentApplicationDate, patentPublicationDate, patentCountry)} was added.");
                        _libraryObjectLogic.AddLibraryObject(idPatent, namePatent, patentApplicationDate.Year, patentCountOfPages, patentNote);
                        break;
                    case "4":
                        _authorLogic.DeleteAuthor(ConsoleIO.DeleteAuthor());
                        ConsoleIO.PrintInformation("Author deleted!");
                        break;
                    case "5":
                        int idDeletedBook = ConsoleIO.DeleteBook();
                        _bookLogic.DeleteBook(idDeletedBook);
                        _libraryObjectLogic.DeleteLibraryObject(idDeletedBook);
                        ConsoleIO.PrintInformation("Book deleted!");
                        break;
                    case "6":
                        int idDeletedNewspaper = ConsoleIO.DeleteNewspaper();
                        _newspaperLogic.DeleteNewspaper(idDeletedNewspaper);
                        _libraryObjectLogic.DeleteLibraryObject(idDeletedNewspaper);
                        ConsoleIO.PrintInformation("Newspaper deleted!");
                        break;
                    case "7":
                        int idDeletedPatent = ConsoleIO.DeletePatent();
                        _patientLogic.DeletePatent(idDeletedPatent);
                        _libraryObjectLogic.DeleteLibraryObject(idDeletedPatent);
                        ConsoleIO.PrintInformation("Patent deleted!");
                        break;
                    case "8":
                        foreach (LibraryObject libraryObject in _libraryObjectLogic.GetAllLibraryObjects())
                        {
                            ConsoleIO.PrintInformation(libraryObject.ToString());
                        }
                        break;
                    case "9":
                        string nameOfLibraryObject = ConsoleIO.GetLibraryObjectByName();
                        ConsoleIO.PrintInformation(_libraryObjectLogic.GetLibraryObjectByName(nameOfLibraryObject).ToString());
                        break;
                    case "10":
                        IEnumerable<LibraryObject> sortedLibraryObjects = _bookLogic.GetSortedBookByPublicationYear();
                        foreach (LibraryObject libraryObject in sortedLibraryObjects)
                        {
                            ConsoleIO.PrintObject(libraryObject);
                        }
                        break;
                    case "11":
                        IEnumerable<LibraryObject> reverseSortedLibraryObjects = _bookLogic.GetReverseSortedBookByPublicationYear();
                        foreach (LibraryObject libraryObject in reverseSortedLibraryObjects)
                        {
                            ConsoleIO.PrintObject(libraryObject);
                        }
                        break;
                    case "12":
                        string authorName = ConsoleIO.GetBooksByAuthor();
                        IEnumerable<Book> books = _bookLogic.GetBooksByNameAuthor(authorName);
                        foreach (var book in books)
                        {
                            ConsoleIO.PrintInformation(book.ToString());
                        }
                        break;
                    case "13":
                        string inventorName = ConsoleIO.GetPatentsByInventor();
                        IEnumerable<Patent> patents = _patientLogic.GetPatentsByNameInventor(inventorName);
                        foreach (var patent in patents)
                        {
                            ConsoleIO.PrintInformation(patent.ToString());
                        }
                        break;
                    case "14":
                        string authorAndInventorName = ConsoleIO.GetBooksByAuthor();
                        IEnumerable<Book> booksList = _bookLogic.GetBooksByNameAuthor(authorAndInventorName);
                        foreach (var book in booksList)
                        {
                            ConsoleIO.PrintInformation(book.ToString());
                        }
                        IEnumerable<Patent> patentsList = _patientLogic.GetPatentsByNameInventor(authorAndInventorName);
                        foreach (var patent in patentsList)
                        {
                            ConsoleIO.PrintInformation(patent.ToString());
                        }
                        break;
                    case "15":
                        string characterSet = ConsoleIO.GetBooksByCharacterSet();
                        IEnumerable<Book> booksByCharacterSet = _bookLogic.GetBooksByCharacterSet(characterSet);
                        foreach (Book book in booksByCharacterSet)
                        {
                            ConsoleIO.PrintInformation(book.ToString());
                        }
                        break;
                    case "16":
                        IEnumerable<IEnumerable<LibraryObject>> libraryObjects = _bookLogic.GroupByPublicationYear();
                        foreach (var groupOfLibraryObjects in libraryObjects)
                        {
                            foreach (var libraryObject in groupOfLibraryObjects)
                            {
                                ConsoleIO.PrintObject(libraryObject);
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
