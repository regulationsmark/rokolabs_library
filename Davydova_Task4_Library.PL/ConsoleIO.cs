﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.PL
{
    public class ConsoleIO
    {
        public static void PrintMenu()
        {
            Console.WriteLine("Enter action: ");
            Console.WriteLine("0 - Add author");
            Console.WriteLine("1 - Add book to repository");
            Console.WriteLine("2 - Add newspaper to repository");
            Console.WriteLine("3 - Add patent to repository");
            Console.WriteLine("4 - Delete author");
            Console.WriteLine("5 - Delete book from repository");
            Console.WriteLine("6 - Delete newspaper from repository");
            Console.WriteLine("7 - Delete patent from repository");
            Console.WriteLine("8 - Show all repository");
            Console.WriteLine("9 - Get library object by name");
            Console.WriteLine("10 - Sort book by publication year");
            Console.WriteLine("11 - Reverse sort book by publication year");
            Console.WriteLine("12 - Get all books by author");
            Console.WriteLine("13 - Get all patents by inventor");
            Console.WriteLine("14 - Get all books and patents by author");
            Console.WriteLine("15 - Get all book by character set with group by publication house");
            Console.WriteLine("16 - Group by publication year");
        }

        public static string EnterAction()
        {
            return Console.ReadLine();
        }

        public static string EnterObjectName()
        {
            Console.WriteLine("Enter name of book");
            string name = Console.ReadLine();
            return name;
        }
        public static string EnterBookISBN()
        {  
            Console.WriteLine("Enter ISBN");
            string iSBN = Console.ReadLine();
            return iSBN;
        }

        public static List<string> EnterAuthors()
        {
            Console.WriteLine("Enter count of authors");
            int countOfAuthors = int.Parse(Console.ReadLine());
            string[] authors = new string[countOfAuthors];
            Console.WriteLine("Enter authors: ");
            for (int i = 0; i < countOfAuthors; i++)
            {
                authors[i] = Console.ReadLine();
            }
            return authors.ToList();
        }

        public static int EnterObjectId()
        {
            Console.WriteLine("Enter id");
            int id = int.Parse(Console.ReadLine());
            return id;
        }

        public static void PrintInformation(string information)
        {
            Console.WriteLine(information);
        }

        public static void PrintObject(object someObject)
        {
            Console.WriteLine(someObject);
        }

        public static string[] EnterAuthorInformation()
        {
            string[] nameAndSurname = new string[2];
            Console.WriteLine("Enter name of author");
            string name = Console.ReadLine();
            nameAndSurname[0] = name;

            Console.WriteLine("Enter surname of author");
            string surname = Console.ReadLine();
            nameAndSurname[1] = surname;

            return nameAndSurname;
        }

        public static string EnterPublishingYear()
        {
            Console.WriteLine("Enter publishing year");
            string publishingYear = Console.ReadLine();
            return publishingYear;
        }

        public static string EnterCountOfPages()
        {
            Console.WriteLine("Enter count of pages");
            string countOfPages = Console.ReadLine();
            return countOfPages;
        }

        public static string EnterNote()
        {
            Console.WriteLine("Enter note");
            string note = Console.ReadLine();
            return note;
        }

        public static string EnterPlaceOfPublication()
        {
            Console.WriteLine("Enter place of publication");
            string placeOfPublication = Console.ReadLine();
            return placeOfPublication;
        }

        public static string EnterPublishingHouse()
        {
            Console.WriteLine("Enter publishing house");
            string publishingHouse = Console.ReadLine();
            return publishingHouse;
        }

        public static int EnterIssueNumber()
        {
            Console.WriteLine("Enter issue number");
            int issueNumber = int.Parse(Console.ReadLine());
            return issueNumber;
        }

        public static DateTime EnterIssueDate()
        {
            Console.WriteLine("Enter issue date");
            DateTime issueDate = DateTime.Parse(Console.ReadLine());
            return issueDate;
        }


        public static string EnterISSNNewspaper()
        {
            Console.WriteLine("Enter ISSN");
            string iSSN = Console.ReadLine();
            return iSSN;
        }

        public static string EnterPatentRegistrationNumber()
        {
            Console.WriteLine("Enter registration number");
            string registrationNumber = Console.ReadLine();
            return registrationNumber;
        }

        public static DateTime EnterApplicationDate()
        {
            Console.WriteLine("Enter application date");
            DateTime applicationDate = DateTime.Parse(Console.ReadLine());
            return applicationDate;
        }

        public static DateTime EnterPublicationDate()
        {
            Console.WriteLine("Enter publication date");
            DateTime publicationDate = DateTime.Parse(Console.ReadLine());
            return publicationDate;
        }

        public static string EnterCountry()
        {
            Console.WriteLine("Enter country");
            string country = Console.ReadLine();
            return country;
        }
        
        public static int DeleteAuthor()
        {
            Console.WriteLine("Enter id author");
            int idAuthor = int.Parse(Console.ReadLine());
            return idAuthor;
        }

        public static int DeleteBook()
        {
            Console.WriteLine("Enter book id");
            int bookId = int.Parse(Console.ReadLine());
            return bookId;
        }

        public static int DeleteNewspaper()
        {
            Console.WriteLine("Enter id newspaper");
            int idNewspaper = int.Parse(Console.ReadLine());
            return idNewspaper;
        }

        public static int DeletePatent()
        {
            Console.WriteLine("Enter id patent");
            int idPatent = int.Parse(Console.ReadLine());
            return idPatent;            
        }

        public static string GetLibraryObjectByName()
        {
            Console.WriteLine("Enter name");
            string name = Console.ReadLine();
            return name;
        }

        public static string GetBooksByAuthor()
        {
            Console.WriteLine("Enter author name");
            string name = Console.ReadLine();
            return name;
        }

        public static string GetPatentsByInventor()
        {
            Console.WriteLine("Enter inventor name");
            string name = Console.ReadLine();
            return name;
        }

        public static string GetBooksByCharacterSet()
        {
            Console.WriteLine("Enter character set as one string");
            string characterSet = Console.ReadLine();
            return characterSet;
        }

    }
}
