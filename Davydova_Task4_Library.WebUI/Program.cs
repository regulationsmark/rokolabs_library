using Davydova_Task4_Library.BLL;
using Davydova_Task4_Library.RealDAL;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using System;


namespace Davydova_Task4_Library.WebUI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            var host = WebHost.CreateDefaultBuilder(args);

            host.ConfigureServices((services) =>
            {
                services
                .AddPatentRealDao()
                .AddNewspaperRealDao()
                .AddAuthorRealDao()
                .AddBookRealDao()
                .AddBaseRealDao()
                .AddLibraryObjectRealDao()
                .AddPublicationRealDao()
                .AddAuthorLogic()
                .AddLibraryObjectLogic()
                .AddPublicationLogic()
                .AddBookLogic()
                .AddNewspaperLogic()
                .AddPatentLogic();
            });

            host.ConfigureAppConfiguration((hostingContext, config) =>
            {
                config
                 .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                 .AddJsonFile("appsettings.json");
            });

            host.ConfigureLogging((hostingContext, loggingBuilder) =>
            {
                loggingBuilder.ClearProviders();

                var logger = new LoggerConfiguration()
                    .MinimumLevel.Debug()
                    .WriteTo.File("data.log")
                    .WriteTo.Console()
                    .CreateLogger();

                loggingBuilder.AddSerilog(logger, dispose: true);
            });

            host.UseStartup<Startup>();

            return host;
        }
    }
}
