﻿using Davydova_Task4_Library.WebUI.Models.BaseViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.WebUI.Models.PatentViewModels
{
    public class GetPatent:GetLibraryObject
    {
        public string Country { get; set; }
        public int RegistrationNumber { get; set; }
        public DateTime? ApplicationDate { get; set; }
        public DateTime PublicationDate { get; set; }
        public List<int> AuthorsId { get; set; } = new List<int>();
    }
}
