﻿using Davydova_Task4_Library.WebUI.Models.BaseViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.WebUI.Models.PatentViewModels
{
    public class CreatePatent:CreateLibraryObject
    {
        [Required]
        [MaxLength(200)]
        public string Country { get; set; }

        [Required]
        public int RegistrationNumber { get; set; }

        [DataType(DataType.Date)]
        public DateTime? ApplicationDate { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime PublicationDate { get; set; }
    }
}
