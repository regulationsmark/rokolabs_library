﻿
using Davydova_Task4_Library.WebUI.Models.AuthorViewModels;
using Davydova_Task4_Library.WebUI.Models.PublicationViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.WebUI.Models.Book
{
    public class CreateBook : CreatePublication
    {
        [MaxLength(18)]
        [MinLength(18)]
        public string ISBN { get; set; }

        public List<Davydova_Task4_Library.Library.OtherEntities.Author> Authors { get; set; } = new List<Davydova_Task4_Library.Library.OtherEntities.Author>();
    }
}
