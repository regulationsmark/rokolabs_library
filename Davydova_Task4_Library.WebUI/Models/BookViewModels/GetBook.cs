﻿using Davydova_Task4_Library.WebUI.Models.AuthorViewModels;
using Davydova_Task4_Library.WebUI.Models.PublicationViewModels;
using System;
using System.Collections.Generic;

namespace Davydova_Task4_Library.WebUI.Models.BookViewModels
{
    public class GetBook:GetPublication
    {
        public string ISBN { get; set; }
        public List<Davydova_Task4_Library.Library.OtherEntities.Author> Authors { get; set; } = new List<Davydova_Task4_Library.Library.OtherEntities.Author>();
    }
}
