﻿using Davydova_Task4_Library.WebUI.Models.PublicationViewModels;
using System.Collections.Generic;
using Davydova_Task4_Library.Library.OtherEntities;

namespace Davydova_Task4_Library.WebUI.Models.BookViewModels
{
    public class EditBook:EditPublication
    {
        public string ISBN { get; set; }
        public List<Davydova_Task4_Library.Library.OtherEntities.Author> Authors { get; set; } = new List<Davydova_Task4_Library.Library.OtherEntities.Author>();
    }
}
