﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Davydova_Task4_Library.Library.OtherEntities;

namespace Davydova_Task4_Library.WebUI.Models.BookViewModels
{
    public class AuthorsByBook
    {
        public int bookId { get; set; }

        public List<Davydova_Task4_Library.Library.OtherEntities.Author> authors = new List<Davydova_Task4_Library.Library.OtherEntities.Author>();
    }
}
