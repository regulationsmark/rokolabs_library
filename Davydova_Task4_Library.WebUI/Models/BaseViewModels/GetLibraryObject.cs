﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.WebUI.Models.BaseViewModels
{
    public class GetLibraryObject
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PublicationYear { get; set; }
        public string CountOfPages { get; set; }
        public string Note { get; set; }
    }
}
