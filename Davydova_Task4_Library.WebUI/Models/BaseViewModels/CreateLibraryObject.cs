﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.WebUI.Models.BaseViewModels
{
    public class CreateLibraryObject
    {
        [Required]
        [MaxLength(300)]
        public string Name { get; set; }

        [Required]
        public string PublicationYear { get; set; }

        [Required]
        public string CountOfPages { get; set; }

        [MaxLength(2000)]
        public string Note { get; set; }
    }
}
