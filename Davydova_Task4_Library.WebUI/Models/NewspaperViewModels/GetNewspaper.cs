﻿using Davydova_Task4_Library.WebUI.Models.PublicationViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.WebUI.Models.NewspaperViewModels
{
    public class GetNewspaper:GetPublication
    {
        public int IssueNumber { get; set; }//?can delete
        public DateTime IssueDate { get; set; }//?can delete
        public string ISSN { get; set; }
    }
}
