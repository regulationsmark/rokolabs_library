﻿using Davydova_Task4_Library.WebUI.Models.PublicationViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.WebUI.Models.NewspaperViewModels
{
    public class CreateNewspaper:CreatePublication
    {
        public int IssueNumber { get; set; }//?can delete

        [Required]
        [DataType(DataType.Date)]//datetime convert?, add compareattribute 
        public DateTime IssueDate { get; set; }

        [MaxLength(14)]
        [MinLength(14)]
        public string ISSN { get; set; }
    }
}
