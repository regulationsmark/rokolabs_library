﻿namespace Davydova_Task4_Library.WebUI.Models.AuthorViewModels
{
    public class EditAuthor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}
