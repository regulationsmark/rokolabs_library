﻿using Davydova_Task4_Library.WebUI.Models.BaseViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.WebUI.Models.PublicationViewModels
{
    public class EditPublication:EditLibraryObject
    {
        public string PlaceOfPublication { get; set; }
        public string PublishingHouse { get; set; }
    }
}
