﻿using Davydova_Task4_Library.WebUI.Models.BaseViewModels;

namespace Davydova_Task4_Library.WebUI.Models.PublicationViewModels
{
    public class GetPublication:GetLibraryObject
    {
        public string PlaceOfPublication { get; set; }
        public string PublishingHouse { get; set; }
    }
}
