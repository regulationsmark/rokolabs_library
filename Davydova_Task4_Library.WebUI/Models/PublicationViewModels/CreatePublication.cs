﻿using Davydova_Task4_Library.WebUI.Models.BaseViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.WebUI.Models.PublicationViewModels
{
    public class CreatePublication: CreateLibraryObject
    {
        [Required]
        [MaxLength(200)]
        public string PlaceOfPublication { get; set; }

        [Required]
        [MaxLength(300)]
        public string PublishingHouse { get; set; }
    }
}
