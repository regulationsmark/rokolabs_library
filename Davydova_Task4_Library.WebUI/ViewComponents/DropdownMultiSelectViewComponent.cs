﻿using Davydova_Task4_Library.BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.WebUI.ViewComponents
{
    public class DropdownMultiSelectViewComponent:ViewComponent
    {
        private IAuthorLogic _authorLogic;

        public DropdownMultiSelectViewComponent(IAuthorLogic authorLogic)
        {
            _authorLogic = authorLogic;
        }

        public IViewComponentResult Invoke()
        {
            ViewBag.ListOfAuthors = new MultiSelectList(_authorLogic.GetAllAuthors(), "Id", "FullName");
            return View();
        }
    }
}
