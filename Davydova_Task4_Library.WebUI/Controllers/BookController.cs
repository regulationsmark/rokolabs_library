﻿using Davydova_Task4_Library.BLL.Implementations;
using Davydova_Task4_Library.BLL.Interfaces;
using Davydova_Task4_Library.WebUI.Models.Book;
using Davydova_Task4_Library.WebUI.Models.BookViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Serilog.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.WebUI.Controllers
{
    public class BookController : Controller
    {
        private IBookLogic _bookLogic;
        private readonly ILogger<BookLogic> _logger;

        public BookController(IBookLogic bookLogic, ILogger<BookLogic> logger)
        {
            _bookLogic = bookLogic;
            _logger = logger;
        }

       // GET: BookController/Details/5
        public ActionResult Details(int id)
        {
            try
            {
                return View(_bookLogic.GetBookById(id));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return RedirectToAction("Index", "LibraryObject");
            }
        }

        // GET: BookController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BookController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateBook book)
        {
            try
            {
                _bookLogic.AddBook(book.Name, book.CountOfPages, book.Note, book.PlaceOfPublication, book.PublishingHouse, book.PublicationYear, book.Authors, book.ISBN);
                return RedirectToAction("Index", "LibraryObject");
            }
            catch (Exception ex)
            {
                _logger.LogError("Creating was wrong" + ex.Message);
                return View();
            }
        }

        // GET: BookController/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                return View(_bookLogic.GetBookById(id));
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Editing was wrong " + ex.Message);
                return RedirectToAction("Index", "LibraryObject");
            }
        }

        // POST: BookController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditBook book)
        {
            try
            {
                _bookLogic.UpdateBook(book.Id, book.Name, book.CountOfPages, book.Note, book.PlaceOfPublication, book.PublishingHouse, book.PublicationYear, book.Authors, book.ISBN);
                return RedirectToAction("Index", "PrintEdition");
            }
            catch
            {
                _logger.LogInformation("Editing was wrong");
                return RedirectToAction("Index", "LibraryObject");
            }
        }

        // GET: BookController/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                return View(_bookLogic.GetBookById(id));
            }
            catch (Exception ex)
            {
                _logger.LogError("Deleting was wrong" + ex.Message);
                return RedirectToAction("Details", "Book", id);
            }
        }

        // POST: BookController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(GetBook book)
        {
            try
            {
                _bookLogic.DeleteBook(book.Id);
                return RedirectToAction("Index", "LibraryObject");
            }
            catch (Exception ex)
            {
                _logger.LogError("Deleting was wrong" + ex.Message);
                return View();
            }
        }
    }
}
