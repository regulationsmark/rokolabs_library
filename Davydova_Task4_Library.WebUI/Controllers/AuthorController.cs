﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Davydova_Task4_Library.Library.OtherEntities;
using Microsoft.Extensions.Logging;
using Davydova_Task4_Library.BLL.Interfaces;
using Davydova_Task4_Library.BLL;
using Davydova_Task4_Library.WebUI.Models.Author;
using Davydova_Task4_Library.WebUI.Models.AuthorViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using Davydova_Task4_Library.WebUI.ViewComponents;

namespace Davydova_Task4_Library.WebUI.Controllers
{
    public class AuthorController : Controller
    {
        private IAuthorLogic _authorLogic;
        private readonly ILogger<AuthorLogic> _logger;

        public AuthorController(IAuthorLogic authorLogic, ILogger<AuthorLogic> logger)
        {
            _authorLogic = authorLogic;
            _logger = logger;
        }

        //GET: AuthorController
        public ActionResult Index()
        {
            var authors = _authorLogic.GetAllAuthors();
            return View(authors);
        }

        // GET: AuthorController/Details/5
        public ActionResult Details(int id)
        {
            try
            {
                return View(_authorLogic.GetAuthorById(id));
            }
            catch (Exception ex)
            {
                _logger.LogError("Detailing was wrong" + ex.Message);
                return RedirectToAction("Index", "Author");
            }
        }

        // GET: AuthorController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AuthorController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateAuthor author)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _authorLogic.AddAuthor(author.Name, author.Surname);
                    return ViewComponent("DropdownMultiSelect");
                }
                catch
                {
                    _logger.LogInformation("Creating was wrong");
                    return View();
                }
            }
            else
            {
                _logger.LogInformation("Model state is not valid");
            }
            return View(author);
        }

        // GET: AuthorController/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                return View(_authorLogic.GetAuthorById(id));
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Editing was wrong" + ex.Message);
                return RedirectToAction("Index", "Author"); 
            }
        }

        // POST: AuthorController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditAuthor author)
        {
            try
            {
                _authorLogic.UpdateAuthor(author.Id, author.Name, author.Surname);
                return RedirectToAction("Index", "Author");
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Editing was wrong" + ex.Message);
                return RedirectToAction();//some redirect
            }
        }

        // GET: AuthorController/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                return View(_authorLogic.GetAuthorById(id));
            }
            catch (Exception ex)
            {
                _logger.LogError("Deleting was wrong" + ex.Message);
                return RedirectToAction("Details", "Author", id);
            }
        }

        // POST: AuthorController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(GetAuthor author)
        {
            try
            {
                _authorLogic.DeleteAuthor(author.Id);
                return RedirectToAction("Index", "Author");
            }
            catch(Exception ex)
            {
                _logger.LogError("Deleting was wrong" + ex.Message);
                return View();
            }
        }
    }
}
