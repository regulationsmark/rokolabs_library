﻿using Davydova_Task4_Library.DAL_Interfaces;
using Davydova_Task4_Library.Library.Documents;
using Davydova_Task4_Library.Library.OtherEntities;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.DAL
{
    public class PatentDao : IPatentDao
    {
        private List<Patent> _patents;
        private ILogger<PatentDao> _logger;
        public PatentDao(ILogger<PatentDao> logger)
        {
            _logger = logger;
            _patents = new List<Patent>();
        }
        public int AddPatent(Patent patent)
        {
            _patents.Add(patent);
            _logger.LogDebug($"{_patents.Count} patents was created.");
            return _patents.Last().Id;
        }

        public bool DeletePatent(int patentId)
        {
            foreach (Patent patent in _patents)
            {
                if (patent.Id == patentId)
                {
                    _logger.LogDebug($"Patent with id {patentId} was deleted.");
                    return _patents.Remove(patent);
                }
            }
            _logger.LogDebug($"Patent with id {patentId} don`t exists.");
            return false;
        }

        public IEnumerable<Patent> GetAllPatents()
        {
            _logger.LogDebug($"{_patents.Count} authors returned. Ids: { JsonConvert.SerializeObject(_patents.Select(p => p.Id)) }");
            return _patents;
        }

        public Patent GetPatentById(int idPatent)
        {
            foreach (Patent patent in _patents)
            {
                if (patent.Id == idPatent)
                {
                    _logger.LogDebug($"Patent with id {idPatent} was recieved.");
                    return patent;
                }
            }
            _logger.LogDebug($"Patent with id {idPatent} don`t exists.");
            return null;
        }

        public IEnumerable<Patent> GetPatentsByInventorId(int inventorId)
        {
            List<Patent> patents = new List<Patent>();
            foreach (Patent patent in _patents)
            {
                foreach(Author inventor in patent.Inventors)
                {
                    if (inventor.Id == inventorId)
                    {
                        patents.Add(patent);
                    }
                }
            }
            return patents;
        }

        public void UpdatePatent(Patent patent)
        {
            foreach (Patent patentItem in _patents)
            {
                if (patentItem.Id == patent.Id)
                {
                    patentItem.Name = patent.Name;
                    patentItem.PublicationYear = patent.PublicationYear;
                    patentItem.CountOfPages = patent.CountOfPages;
                    patentItem.Note = patent.Note;
                    patentItem.Inventors = patent.Inventors;
                    patentItem.PublicationDate = patent.PublicationDate;
                    patentItem.RegistrationNumber = patent.RegistrationNumber;
                }
            }
        }
    }
}
