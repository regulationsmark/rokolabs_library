﻿using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.Library.OtherEntities;
using Davydova_Task4_Library.Library.Publications;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.DAL
{
    public class BookDao:IBookDao
    {
        private List<Book> _books;
        private ILogger<BookDao> _logger;
        public BookDao(ILogger<BookDao> logger)
        {
            _logger = logger;
            _books = new List<Book>();
        }
        public Book GetBookById(int bookId)
        {
            foreach (Book book in _books)
            {
                if (book.Id == bookId)
                {
                    _logger.LogDebug($"Book with id {bookId} was recieved.");
                    return book;
                }
            }
            _logger.LogDebug($"Book with id {bookId} don`t exists.");
            return null;
        }
            

        public int AddBook(Book book)
        {
            _books.Add(book);
            _logger.LogDebug($"{_books.Count} books was created.");
            return _books.Last().Id;
        }

        public bool DeleteBook(int bookId)
        {
            foreach (Book book in _books)
            {
                if (book.Id == bookId)
                {
                    _logger.LogDebug($"Book with id {bookId} was deleted.");
                    return _books.Remove(book);
                }
            }
            _logger.LogDebug($"Book with id {bookId} don`t exists.");
            return false;
        }

        public IEnumerable<Book> GetAllBooks()
        {
            _logger.LogDebug($"{_books.Count} books returned. Ids: { JsonConvert.SerializeObject(_books.Select(p => p.Id)) }");
            return _books;
        }

        public void UpdateBook(Book book)
        {
            foreach (Book bookItem in _books)
            {
                if (bookItem.Id == book.Id)
                {
                    bookItem.Name = book.Name;
                    bookItem.PublicationYear = book.PublicationYear;
                    bookItem.CountOfPages = book.CountOfPages;
                    bookItem.Note = book.Note;
                    bookItem.PlaceOfPublication = book.PlaceOfPublication;
                    bookItem.PublishingHouse = book.PublishingHouse;
                    bookItem.Authors = book.Authors;
                    bookItem.ISBN = book.ISBN;
                }
            }
        }

        public IEnumerable<Book> GetBooksByAuthorId(int authorId)
        {
            List<Book> books = new List<Book>();
            bool checkBook = false;
            foreach (Book bookItem in _books)
            {
                checkBook = false;
                foreach (Author author in bookItem.Authors)
                {
                    if (author.Id == authorId)
                    {
                        checkBook = true;
                    }
                }
                if (checkBook)
                {
                    books.Add(bookItem);
                }
            }
            return books;
        }

        public IEnumerable<Author> GetAuthorsByBookId(int bookId)
        {
            List<Author> authors = new List<Author>();
            foreach (Book book in _books)
            {
                if (book.Id == bookId)
                {
                    foreach (Author author in book.Authors)
                    {
                        authors.Add(author);
                    }
                }
            }
            return authors;
        }
    }
}
