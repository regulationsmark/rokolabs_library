﻿using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.DAL_Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Davydova_Task4_Library.DAL
{
    public static class DaoConfigurator
    {
        public static IServiceCollection AddAuthorDao(this IServiceCollection services)
        {
            return services.AddTransient<IAuthorDao, AuthorDao>();
        }

        public static IServiceCollection AddBaseDao(this IServiceCollection services)
        {
            return services.AddTransient<IBaseDao, BaseDao>();
        }

        public static IServiceCollection AddBookDao(this IServiceCollection services)
        {
            return services.AddTransient<IBookDao, BookDao>();
        }

        public static IServiceCollection AddLibraryObjectDao(this IServiceCollection services)
        {
            return services.AddTransient<ILibraryObject, LibraryDao>();
        }
        public static IServiceCollection AddNewspaperDao(this IServiceCollection services)
        {
            return services.AddTransient<INewspaperDao, NewspaperDao>();
        }

        public static IServiceCollection AddPatentDao(this IServiceCollection services)
        {
            return services.AddTransient<IPatentDao, PatentDao>();
        }
        public static IServiceCollection AddPublicationDao(this IServiceCollection services)
        {
            return services.AddTransient<IPublicationDao, PublicationDao>();
        }
    }
}
