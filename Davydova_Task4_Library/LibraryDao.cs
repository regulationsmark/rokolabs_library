﻿using Davydova_Task4_Library.DAL_Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.DAL
{
    public class LibraryDao : ILibraryObject
    {
        private List<LibraryObject> _libraryObjects;
        private ILogger<LibraryDao> _logger;

        public LibraryDao(ILogger<LibraryDao> logger)
        {
            _logger = logger;
            _libraryObjects = new List<LibraryObject>();
        }
        public int AddLibraryObject(LibraryObject libraryObject)
        {
            _libraryObjects.Add(libraryObject);
            _logger.LogDebug($"{_libraryObjects.Count} library objects was created.");
            return _libraryObjects.Last().Id;
        }

        public bool DeleteLibraryObject(int libraryObjectId)
        {
            foreach (LibraryObject libraryObject in _libraryObjects)
            {
                if (libraryObject.Id == libraryObjectId)
                {
                    _logger.LogDebug($"Library object with id {libraryObjectId} was deleted.");
                    return _libraryObjects.Remove(libraryObject);
                }
            }
            _logger.LogDebug($"Library object with id {libraryObjectId} don`t exists.");
            return false;
        }

        public IEnumerable<LibraryObject> GetAllLibraryObjects()
        {
            _logger.LogDebug($"{_libraryObjects.Count} library objects returned. Ids: { JsonConvert.SerializeObject(_libraryObjects.Select(p => p.Id)) }");
            return _libraryObjects;
        }

        public LibraryObject GetLibraryObjectById(int idLibraryObject)
        {
            foreach (LibraryObject libraryObject in _libraryObjects)
            {
                if (libraryObject.Id == idLibraryObject)
                {
                    _logger.LogDebug($"Library object with id {idLibraryObject} was recieved.");
                    return libraryObject;
                }
            }
            _logger.LogDebug($"Library object with id {idLibraryObject} don`t exists.");
            return null;
        }

        public LibraryObject GetLibraryObjectByName(string name)
        {
            foreach (LibraryObject libraryObject in _libraryObjects)
            {
                if (libraryObject.Name == name)
                {
                    _logger.LogDebug($"Library object with name {name} was recieved.");
                    return libraryObject;
                }
            }
            _logger.LogDebug($"Library object with name {name} don`t exists.");
            return null;
        }

        public void UpdateLibraryObject(LibraryObject libraryObject)
        {
            foreach (LibraryObject libraryObject1 in _libraryObjects)
            {
                if (libraryObject1.Id == libraryObject.Id)
                {
                    libraryObject1.Name = libraryObject.Name;
                    libraryObject1.CountOfPages = libraryObject.CountOfPages;
                    libraryObject1.Note = libraryObject.Note;
                    libraryObject1.PublicationYear = libraryObject.PublicationYear;
                }
            }
        }
    }
}
