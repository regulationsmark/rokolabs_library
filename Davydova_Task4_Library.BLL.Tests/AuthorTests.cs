﻿using Davydova_Task4_Library.BLL.Interfaces;
using Davydova_Task4_Library.BLL.Validations;
using Davydova_Task4_Library.DAL;
using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.Library.OtherEntities;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System;
using System.Linq;

namespace Davydova_Task4_Library.BLL.Tests
{
    [TestFixture]
    public class AuthorTests
    {
        private IAuthorLogic _authorLogic;

        [SetUp]
        public void StartUp()
        {
            var fakeAuthorDaoLogger = new Mock<ILogger<AuthorDao>>();
            var fakeAuthorValidationLogger = new Mock<ILogger<AuthorLogic>>();
            IAuthorDao authorDao = new AuthorDao(fakeAuthorDaoLogger.Object);
            _authorLogic = new AuthorLogic(authorDao, fakeAuthorValidationLogger.Object);
        }

        [Test]
        public void AddAuthorTest()
        {
            var result = _authorLogic.GetAllAuthors();
            var count = result.Count();

            int id = 1;
            string name = "Ivan";
            string surname = "Ivanov";            
            int authorId = _authorLogic.AddAuthor(id, name, surname);

            result = _authorLogic.GetAllAuthors();
            Assert.AreEqual(count + 1, result.Count());
            Assert.AreEqual(id, authorId);
        }

        [Test]
        public void AddWhiteSpaceAuthorTest()
        {
            var result = _authorLogic.GetAllAuthors();
            var count = result.Count();

            int id = 2;
            _authorLogic.AddAuthor(id, " ", " ");

            result = _authorLogic.GetAllAuthors();
            Assert.AreEqual(count, result.Count());
        }

        [Test]
        public void GetAllAuthorsTest()
        {
            int id = 1;
            string name = "Ivan";
            string surname = "Ivanov";
            _authorLogic.AddAuthor(id, name, surname);
            var result = _authorLogic.GetAllAuthors();
            Assert.IsNotEmpty(result);
        }

        [Test]
        public void GetNullAuthorByIdTest()
        {
            int id = 4;
            var result = _authorLogic.GetAuthorById(id);
            Assert.AreEqual(null, result);
        }

        [Test]
        public void GetAuthorByIdTest()
        {
            int id = 1;
            string name = "Ivan";
            string surname = "Ivanov";
            _authorLogic.AddAuthor(id, name, surname);

            int newId = 1;
            var result = _authorLogic.GetAuthorById(newId);
            Assert.AreEqual(newId, result.Id);
        }

        [Test]
        public void DeleteAuthorTest()
        {
            int id = 1;
            string name = "Ivan";
            string surname = "Ivanov";
            _authorLogic.AddAuthor(id, name, surname);

            int newId = 1;
            var result = _authorLogic.GetAllAuthors();
            var count = result.Count();

            bool isDelete = _authorLogic.DeleteAuthor(newId);

            result = _authorLogic.GetAllAuthors();
            Assert.AreEqual(count - 1, result.Count());
            Assert.IsTrue(isDelete);
        }

        [Test]
        public void DeleteNonExistentAuthorTest()
        {
            int id = 4;
            var result = _authorLogic.GetAllAuthors();
            var count = result.Count();

            bool isDelete = _authorLogic.DeleteAuthor(id);

            result = _authorLogic.GetAllAuthors();
            Assert.AreEqual(count, result.Count());
            Assert.IsFalse(isDelete);
        }
    }
}
