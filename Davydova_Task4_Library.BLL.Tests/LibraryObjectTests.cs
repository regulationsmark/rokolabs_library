﻿using Davydova_Task4_Library.BLL.Interfaces;
using Davydova_Task4_Library.DAL;
using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.DAL_Interfaces;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.BLL.Tests
{
    [TestFixture]
    public class LibraryObjectTests
    {
        private ILibraryObjectLogic _libraryObjectLogic;

        [SetUp]
        public void StartUp()
        {
            var fakeLibraryObjectDaoLogger = new Mock<ILogger<LibraryDao>>();
            var fakeBaseDaoLogger = new Mock<ILogger<BaseDao>>();
            var fakeLibraryObjectValidationLogger = new Mock<ILogger<LibraryObjectLogic>>();
            ILibraryObject authorDao = new LibraryDao(fakeLibraryObjectDaoLogger.Object);
            IBaseDao baseDao = new BaseDao(fakeBaseDaoLogger.Object);
            _libraryObjectLogic = new LibraryObjectLogic(authorDao, baseDao, fakeLibraryObjectValidationLogger.Object);
        }

        [Test]
        public void AddLibraryTest()
        {
            var result = _libraryObjectLogic.GetAllLibraryObjects();
            var count = result.Count();

            int id = 1;
            string name = "Master and Margarita";
            int publicationYear = 2021;
            string countOfPages = "480";
            string note = "The novel belongs to unfinished works";

            int libraryObjectId = _libraryObjectLogic.AddLibraryObject(id, name, publicationYear, countOfPages, note);

            result = _libraryObjectLogic.GetAllLibraryObjects();
            Assert.AreEqual(count + 1, result.Count());
            Assert.AreEqual(id, libraryObjectId);
        }

        [Test]
        public void AddWhiteSpaceLibraryTest()
        {
            var result = _libraryObjectLogic.GetAllLibraryObjects();
            var count = result.Count();

            int id = 2;
            string name = " ";
            int publicationYear = 2021;
            string countOfPages = " ";
            string note = "The novel belongs to unfinished works";

            _libraryObjectLogic.AddLibraryObject(id, name, publicationYear, countOfPages, note);

            result = _libraryObjectLogic.GetAllLibraryObjects();
            Assert.AreEqual(count, result.Count());
        }

        [Test]
        public void GetAllLibraryObjectsTest()
        {
            int id = 1;
            string name = "Master and Margarita";
            int publicationYear = 2021;
            string countOfPages = "480";
            string note = "The novel belongs to unfinished works";

            int libraryObjectId = _libraryObjectLogic.AddLibraryObject(id, name, publicationYear, countOfPages, note);

            var result = _libraryObjectLogic.GetAllLibraryObjects();
            Assert.IsNotEmpty(result);
        }

        [Test]
        public void DeleteLibraryObjectTest()
        {
            int id = 1;
            string name = "Master and Margarita";
            int publicationYear = 2021;
            string countOfPages = "480";
            string note = "The novel belongs to unfinished works";

            int libraryObjectId = _libraryObjectLogic.AddLibraryObject(id, name, publicationYear, countOfPages, note);

            int newId = 1;
            var result = _libraryObjectLogic.GetAllLibraryObjects();
            var count = result.Count();

            bool isDelete = _libraryObjectLogic.DeleteLibraryObject(newId);

            result = _libraryObjectLogic.GetAllLibraryObjects();
            Assert.AreEqual(count - 1, result.Count());
            Assert.IsTrue(isDelete);
        }

        [Test]
        public void DeleteNonExistentLibraryObjectTest()
        {
            int id = 2;
            var result = _libraryObjectLogic.GetAllLibraryObjects();
            var count = result.Count();

            bool isDelete = _libraryObjectLogic.DeleteLibraryObject(id);

            result = _libraryObjectLogic.GetAllLibraryObjects();
            Assert.AreEqual(count, result.Count());
            Assert.IsFalse(isDelete);
        }

        [Test]
        public void GetLibraryObjectByIdTest()
        {
            int id = 1;
            string name = "Master and Margarita";
            int publicationYear = 2021;
            string countOfPages = "480";
            string note = "The novel belongs to unfinished works";

            int libraryObjectId = _libraryObjectLogic.AddLibraryObject(id, name, publicationYear, countOfPages, note);

            int newId = 1;
            var result = _libraryObjectLogic.GetLibraryObjectById(newId);
            Assert.AreEqual(id, result.Id);
        }

        [Test]
        public void GetNullLibraryObjectByIdTest()
        {
            int id = 2;
            var result = _libraryObjectLogic.GetLibraryObjectById(id);
            Assert.AreEqual(null, result);
        }
    }
}
