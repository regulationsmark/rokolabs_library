﻿using Davydova_Task4_Library.BLL.Interfaces;
using Davydova_Task4_Library.DAL;
using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.DAL_Interfaces;
using Davydova_Task4_Library.Library.OtherEntities;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.BLL.Tests
{
    [TestFixture]
    public class PatentTests
    {
        private IPatientLogic _patentLogic;
        private IAuthorLogic _authorLogic;

        [SetUp]
        public void StartUp()
        {
            var fakePatentDaoLogger = new Mock<ILogger<PatentDao>>();
            var fakebaseDaoLogger = new Mock<ILogger<BaseDao>>();
            var fakeAuthorDaoLogger = new Mock<ILogger<AuthorDao>>();
            var fakePatentLogicLogger = new Mock<ILogger<PatentLogic>>();
            var fakeAuthorValidationLogger = new Mock<ILogger<AuthorLogic>>();
            IPatentDao patentDao = new PatentDao(fakePatentDaoLogger.Object);
            IBaseDao baseDao = new BaseDao(fakebaseDaoLogger.Object);
            IAuthorDao authorDao = new AuthorDao(fakeAuthorDaoLogger.Object);
            _patentLogic = new PatentLogic(patentDao, baseDao, authorDao, fakePatentLogicLogger.Object);
            _authorLogic = new AuthorLogic(authorDao, fakeAuthorValidationLogger.Object);
        }

        /*[Test]
        public void AddPatentTest()
        {
            var result = _patentLogic.GetAllPatents();
            var count = result.Count();

            List<Author> inventors = new List<Author>();

            int idAuthor1 = 1;
            string nameAuthor1 = "Stephen";
            string surnameAuthor1 = "Lynch";

            int idAuthor2 = 2;
            string nameAuthor2 = "Tyson";
            string surnameAuthor2 = "Manullang";

            int idAuthor3 = 3;
            string nameAuthor3 = "Emery";
            string surnameAuthor3 = "Stanford";

            Author author1 = new(idAuthor1, nameAuthor1, surnameAuthor1);
            Author author2 = new(idAuthor2, nameAuthor2, surnameAuthor2);
            Author author3 = new(idAuthor3, nameAuthor3, surnameAuthor3);
            inventors.Add(author1);
            inventors.Add(author2);
            inventors.Add(author3);

            int id = 1;
            string name = "Apples self-deploying screen protector";
            string countOfPages = "25";
            string note = "Never crack your screen again!";
            string placeOfPublication = "San Francisco";
            string registrationNumber = "123";
            DateTime applicationDate = new DateTime(2014, 05, 02);
            DateTime publicationDate = new DateTime(2015, 05, 02);
            string country = "USA";

            int patentId = _patentLogic.AddPatent(id, name, countOfPages, note, placeOfPublication, inventors, registrationNumber, applicationDate, publicationDate, country);

            result = _patentLogic.GetAllPatents();
            Assert.AreEqual(count + 1, result.Count());
            Assert.AreEqual(id, patentId);
        }*/

        [Test]
        public void AddWhiteSpacePatentTest()
        {
            var result = _patentLogic.GetAllPatents();
            var count = result.Count();

            List<Author> inventors = new List<Author>();

            int idAuthor = 1;
            string nameAuthor = "Stephen";
            string surnameAuthor = "Lynch";

            Author author = new(idAuthor, nameAuthor, surnameAuthor);
            inventors.Add(author);

            int id = 1;
            string name = " ";
            string countOfPages = " ";
            string note = " ";
            string registrationNumber = " ";
            DateTime applicationDate = new DateTime();
            DateTime publicationDate = new DateTime();
            string country = " ";

            _patentLogic.AddPatent(id, name, countOfPages, note, inventors, registrationNumber, applicationDate, publicationDate, country);

            result = _patentLogic.GetAllPatents();
            Assert.AreEqual(count, result.Count());
        }

        /*[Test]
        public void GetAllPatentsTest()
        {
            var result = _patentLogic.GetAllPatents();
            Assert.IsNotEmpty(result);
        }*/

        /*[Test]
        public void DeletePatentTest()
        {
            int id = 1;
            var result = _patentLogic.GetAllPatents();
            var count = result.Count();

            bool isDelete = _patentLogic.DeletePatent(id);

            result = _patentLogic.GetAllPatents();
            Assert.AreEqual(count - 1, result.Count());
            Assert.IsTrue(isDelete);
        }*/

        [Test]
        public void DeleteNonExistentPatentTest()
        {
            int id = 2;
            var result = _patentLogic.GetAllPatents();
            var count = result.Count();

            bool isDelete = _patentLogic.DeletePatent(id);

            result = _patentLogic.GetAllPatents();
            Assert.AreEqual(count, result.Count());
            Assert.IsFalse(isDelete);
        }

        /*[Test]
        public void GetPatentByIdTest()
        {
            int id = 1;
            var result = _patentLogic.GetPatentById(id);
            Assert.AreEqual(id, result.Id);
        }*/

        [Test]
        public void GetNullPatentByIdTest()
        {
            int id = 2;
            var result = _patentLogic.GetPatentById(id);
            Assert.AreEqual(null, result);
        }

        [Test]
        public void GetNonExistentPatentsByInventorNameTest()
        {
            int id = 1;
            string name = "Ivan";
            string surname = "Ivanov";
            _authorLogic.AddAuthor(id, name, surname);

            var result = _patentLogic.GetPatentsByNameInventor(name);
            Assert.IsEmpty(result);
        }
    }
}
