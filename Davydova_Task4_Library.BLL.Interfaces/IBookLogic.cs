﻿using Davydova_Task4_Library.Library.OtherEntities;
using Davydova_Task4_Library.Library.Publications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.BLL.Interfaces
{
    public interface IBookLogic
    {
        public int AddBook(int id, string name, string countOfPages, string note, string placeOfPublication, string publishingHouse, string publishingYear, List<Author> authors, string iSBN);
        public int AddBook(string name, string countOfPages, string note, string placeOfPublication, string publishingHouse, string publishingYear, List<Author> authors, string iSBN);
        public bool DeleteBook(int id);
        public IEnumerable<Book> GetAllBooks();
        public Book GetBookById(int id);
        public IEnumerable<Book> GetBooksByName(string name);
        public IEnumerable<Author> GetAuthorsByBookId(int id);
        public IEnumerable<LibraryObject> GetSortedBookByPublicationYear();
        public IEnumerable<LibraryObject> GetReverseSortedBookByPublicationYear();

        public IEnumerable<Book> GetBooksByIdAuthor(int id);
        public IEnumerable<Book> GetBooksByNameAuthor(string name);
        public IEnumerable<Book> GetBooksByCharacterSet(string characterSet);

        public IEnumerable<IEnumerable<Book>> GroupByPublicationYear();

        public void UpdateBook(int id, string name, string countOfPages, string note, string placeOfPublication, string publishingHouse, string publishingYear, List<Author> authors, string iSBN);

    }
}
