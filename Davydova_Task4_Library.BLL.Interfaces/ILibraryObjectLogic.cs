﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.BLL.Interfaces
{
    public interface ILibraryObjectLogic
    {
        int AddLibraryObject(int id, string name, int publicationYear, string countOfPages, string note);
        void UpdateLibraryObject(int id, string name, int publicationYear, string countOfPages, string note);

        bool DeleteLibraryObject(int libraryObjectId);

        LibraryObject GetLibraryObjectById(int libraryObjectId);

        LibraryObject GetLibraryObjectByName(string name);

        IEnumerable<LibraryObject> GetAllLibraryObjects();
    }
}
