﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Davydova_Task4_Library.Library.Publications;

namespace Davydova_Task4_Library.BLL.Interfaces
{
    public interface IPublicationLogic
    {
        int AddPublication(int id, string name, string publicationYear, string countOfPages, string note, string placeOfPublication, string publishingHouse);

        bool DeletePublication(int publicationId);

        Publication GetPublicationById(int idPublication);

        IEnumerable<Publication> GetAllPublication();
    }
}
