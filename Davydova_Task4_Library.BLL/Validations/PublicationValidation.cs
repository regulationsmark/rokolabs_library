﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Davydova_Task4_Library.BLL.Validations
{
    public class PublicationValidation
    {
        public List<string> exceptionsList = new List<string>();

        public bool PublishingHouseValidation(string publishingHouse)
        {
            bool result = true;
            if (string.IsNullOrWhiteSpace(publishingHouse))
            {
                result = false;
                exceptionsList.Add("Publishing house cannot be empty.");
            }
            else if (publishingHouse.Length > 300)
            {
                result = false;
                exceptionsList.Add("Publishing house cannot be more than 300.");
            }
            return result;
        }

        public bool PublicationYearValidation(string publicationYear)
        {
            bool result = true;
            var now = DateTime.UtcNow;
            int year;
            if (!string.IsNullOrWhiteSpace(publicationYear))
            {
                bool success = Int32.TryParse(publicationYear, out year);
                if (success)
                {
                    if (year > now.Year)
                    {
                        result = false;
                        exceptionsList.Add("Publication year must be less or equal to current year.");
                    }
                    if (year < 1400)
                    {
                        result = false;
                        exceptionsList.Add("Publication year must be less than 1400.");
                    }
                }
                else
                {
                    result = false;
                    exceptionsList.Add("Publication year must be digits.");
                }
            }
            else
            {
                result = false;
                exceptionsList.Add("Publication year cannot be empty.");
            }
            return result;
        }

        private bool PlaceOfPublicationValidation(string placeOfPublication)
        {
            bool result = true;
            if (string.IsNullOrWhiteSpace(placeOfPublication))
            {
                result = false;
                exceptionsList.Add("Place of publication cannot be empty.");
            }
            else if (placeOfPublication.Length > 200)
            {
                result = false;
                exceptionsList.Add("Place of publication cannot be more than 200 symbols");
            }
            else
            {
                var englishAlphabet = @"(^[A-Z][a-z]*((-[A-Z][a-z]*)?|(-[a-z]+-[A-Z][a-z]*)?|( ([A-Z]|[a-z])[a-z]*)*)$)";
                var russianAlphabet = @"(^[А-ЯЁ][а-яё]*((-[А-ЯЁ][а-яё]*)?|(-[а-яё]+-[А-ЯЁ][а-яё]*)?|( ([А-ЯЁ]|[а-яё])[а-яё]*)*)$)";

                if (!Regex.IsMatch(placeOfPublication, russianAlphabet) && !Regex.IsMatch(placeOfPublication, englishAlphabet))
                {
                    result = false;
                    exceptionsList.Add("The place of publication can only support Russian or Latin letters. May contain hyphens and spaces.");
                }
            }
            return result;
        }

        public bool ExecutePublicationValidation(string publishingHouse, string publicationYear, string placeOfPublication)
        {
            return PublishingHouseValidation(publishingHouse) && PublicationYearValidation(publicationYear) && PlaceOfPublicationValidation(placeOfPublication);
        }

    }
}
