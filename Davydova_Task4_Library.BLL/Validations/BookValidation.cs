﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.ComponentModel.DataAnnotations;

namespace Davydova_Task4_Library.BLL.Validations
{
    public class BookValidation
    {
        public List<string> exceptionsList = new List<string>();

        public bool ISBNValidation(string iSBN)
        {
            bool result = true;
            if (!string.IsNullOrWhiteSpace(iSBN))
            {
                if (iSBN.Length != 18)
                {
                    result = false;
                    exceptionsList.Add("ISBN length must be equal to 18.");
                }
                else
                {
                    var regex = new Regex(@"^ISBN ([0-7]|([8][0-9])|([9][0-4])|([9][5-8][0-9])|([9][9][0-3])|([9]{2}[4-8][0-9])|([9]{3}[0-9][0-9]))-([0-9]{1,7})-([0-9]{1,7})-([0-9]|X)$");
                    if (!regex.IsMatch(iSBN))
                    {
                        result = false;
                        exceptionsList.Add("Incorrect ISBN.");
                    }
                }
            }
            return result;
        }

        public bool ExecuteBookValidation(string iSBN)
        {
            if (!ISBNValidation(iSBN))
            {
                throw new ValidationException();
            }
            return true;
        }

    }
}
