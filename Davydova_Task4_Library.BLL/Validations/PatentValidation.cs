﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Davydova_Task4_Library.Library.OtherEntities;

namespace Davydova_Task4_Library.BLL.Validations
{
    public class PatentValidation
    {
        public List<string> exceptionsList = new List<string>();

        public bool CountryValidation(string country)
        {
            bool result = true;
            if (string.IsNullOrWhiteSpace(country))
            {
                result = false;
                exceptionsList.Add("Country cannot be empty");
            }
            else if (country.Length > 200)
            {
                result = false;
                exceptionsList.Add("Country length cannot be more than 200.");
            }
            else
            {
                var englishAlphabet = @"(^[A-Z][a-z]*((-[A-Z][a-z]*)?|(-[a-z]*-[A-Z][a-z]*)?|( ([A-Z]|[a-z])[a-z]*)*)$)";
                var russianAlphabet = @"(^[А-ЯЁ][а-яё]*((-[А-ЯЁ][а-яё]*)?|(-[а-яё]*-[А-ЯЁ][а-яё]*)?|( ([А-ЯЁ]|[а-яё])[а-яё]*)*)$|(^[A-Z]*$)|(^[А-ЯЁ]*$))";

                if (!Regex.IsMatch(country, russianAlphabet) && !Regex.IsMatch(country, englishAlphabet))
                {
                    result = false;
                    exceptionsList.Add("The country can only support Russian or Latin letters, it must be written with a capital letter. Can be written as abbreviation.");
                }
            }
            return result;
        }

        public bool RegistrationNumberValidation(string registrationNumber)
        {
            bool result = true;
            int number;
            if (!string.IsNullOrWhiteSpace(registrationNumber))
            {
                if (registrationNumber.Length > 9)
                {
                    bool success = Int32.TryParse(registrationNumber, out number);
                    if (!success)
                    {
                        result = false;
                        exceptionsList.Add("Registration number must be digits.");
                    }
                }
                else
                {
                    result = false;
                    exceptionsList.Add("Registration number length cannot be more than 9 and must be digits.");
                }
            }  
            else
            {
                result = false;
                exceptionsList.Add("Registration number cannot be empty.");
            }
            return result;
        }

        public bool ApplicationDateValidation(DateTime? applicationDate)
        {
            bool result = true;
            var now = DateTime.UtcNow;
            if (applicationDate != null)
            {
                if (applicationDate > now)
                {
                    result = false;
                    exceptionsList.Add("Application date cannot be more than the current date.");
                }
                if (applicationDate.Value.Year < 1474)
                {
                    result = false;
                    exceptionsList.Add("Publication date cannot be less than 1474.");
                }
            }
            return result;
        }

        public bool PublicationDateValidation(DateTime? publicationDate, DateTime applicationDate)
        {
            bool result = true;
            var now = DateTime.UtcNow;
            if (publicationDate != null)
            {
                if (publicationDate < applicationDate)
                {
                    result = false;
                    exceptionsList.Add("Publication date cannot be less than the application date.");
                }
                if (publicationDate > now)
                {
                    result = false;
                    exceptionsList.Add("Publication date cannot be more than current date.");
                }
                if (publicationDate.Value.Year < 1474)
                {
                    result = false;
                    exceptionsList.Add("Publication date cannot be less than 1474.");
                }
            }
            else
            {
                result = false;
                exceptionsList.Add("Publication date is required field.");
            }
            return result;
        }

        public bool InventorsValidation(List<Author> inventors)
        {
            bool result = true;
            if (!inventors.Any())
            {
                result = false;
                exceptionsList.Add("At least one inventor must be specified.");
            }
            return result;
        }

        public bool ExecutePatentValidation(string country, string registrationNumber, DateTime applicationDate, DateTime publicationDate, List<Author> inventors)
        {
            return CountryValidation(country) && RegistrationNumberValidation(registrationNumber) && ApplicationDateValidation(applicationDate) && PublicationDateValidation(publicationDate, applicationDate) && InventorsValidation(inventors);
        }
    }
}
