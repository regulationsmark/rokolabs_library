﻿using Davydova_Task4_Library.BLL.Interfaces;
using Davydova_Task4_Library.BLL.Validations;
using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.Library.OtherEntities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.BLL
{
    public class AuthorLogic:IAuthorLogic
    {
        private IAuthorDao _authorDao;
        private ILogger<AuthorLogic> _logger;

        public AuthorLogic(IAuthorDao authorDao, ILogger<AuthorLogic> logger)
        {
            _authorDao = authorDao;
            _logger = logger;
        }

        public int AddAuthor(int id, string name, string surname)
        {
            AuthorValidation authorValidation = new AuthorValidation();
            try
            {
                authorValidation.ExecuteAuthorValidation(name, surname);
                Author author = new Author(id, name, surname);
                return _authorDao.AddAuthor(author);
            }
            catch(ValidationException)
            {
                StringBuilder failedValidation = new StringBuilder();
                foreach (var exception in authorValidation._exceptionsList)
                {
                    failedValidation.Append(exception + Environment.NewLine);
                }
                //ConsoleIO.PrintInformation(failedValidation.ToString());
                _logger.LogInformation($"User enters invalid data.");
            }
            
            return 0;
        }

        public int AddAuthor(string name, string surname)
        {
            AuthorValidation authorValidation = new AuthorValidation();
            try
            {
                authorValidation.ExecuteAuthorValidation(name, surname);
                Author author = new Author(name, surname);
                return _authorDao.AddAuthor(author);
            }
            catch (ValidationException)
            {
                StringBuilder failedValidation = new StringBuilder();
                foreach (var exception in authorValidation._exceptionsList)
                {
                    failedValidation.Append(exception + Environment.NewLine);
                }
                //ConsoleIO.PrintInformation(failedValidation.ToString());
                _logger.LogInformation($"User enters invalid data.");
            }

            return 0;
        }

        public bool DeleteAuthor(int authorId)
        {
            return _authorDao.DeleteAuthor(authorId);
        }

        public IEnumerable<Author> GetAllAuthors()
        {
            return _authorDao.GetAllAuthors();
        }

        public Author GetAuthorById(int idAuthor)
        {
            return _authorDao.GetAuthorById(idAuthor);
        }

        public void UpdateAuthor(int id, string name, string surname)
        {
            AuthorValidation authorValidation = new AuthorValidation();
            try
            {
                authorValidation.ExecuteAuthorValidation(name, surname);
                Author author = new Author(id, name, surname);
                _authorDao.AddAuthor(author);
            }
            catch (ValidationException)
            {
                StringBuilder failedValidation = new StringBuilder();
                foreach (var exception in authorValidation._exceptionsList)
                {
                    failedValidation.Append(exception + Environment.NewLine);
                }
                //ConsoleIO.PrintInformation(failedValidation.ToString());
                _logger.LogInformation($"User enters invalid data.");
            }
        }
    }
}
