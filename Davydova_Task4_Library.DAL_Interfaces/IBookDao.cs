﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Davydova_Task4_Library.Library.OtherEntities;
using Davydova_Task4_Library.Library.Publications;

namespace Davydova_Task4_Library.DAL.Interfaces
{
    public interface IBookDao
    {
        Book GetBookById(int bookId);

        int AddBook(Book book);

        bool DeleteBook(int bookId);

        IEnumerable<Book> GetAllBooks();

        void UpdateBook(Book book);
        IEnumerable<Book> GetBooksByAuthorId(int authorId);

        IEnumerable<Author> GetAuthorsByBookId(int bookId);

    }
}
