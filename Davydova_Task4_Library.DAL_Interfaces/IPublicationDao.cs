﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Davydova_Task4_Library.Library.Publications;

namespace Davydova_Task4_Library.DAL_Interfaces
{
    public interface IPublicationDao
    {
        Publication GetPublicationById(int publicationId);

        int AddPublication(Publication publication);

        bool DeletePublication(int publicationId);

        IEnumerable<Publication> GetAllPublications();
        void UpdatePublication(Publication publication);
    }
}
