﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Davydova_Task4_Library.Library.Documents;

namespace Davydova_Task4_Library.DAL_Interfaces
{
    public interface IPatentDao
    {
        Patent GetPatentById(int idPatent);

        int AddPatent(Patent patent);

        bool DeletePatent(int patentId);

        IEnumerable<Patent> GetAllPatents();

        IEnumerable<Patent> GetPatentsByInventorId(int inventorId);

        void UpdatePatent(Patent patent);
    }
}
