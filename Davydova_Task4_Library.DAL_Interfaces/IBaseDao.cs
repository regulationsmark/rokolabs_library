﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Davydova_Task4_Library.Library.Documents;
using Davydova_Task4_Library.Library.OtherEntities;
using Davydova_Task4_Library.Library.Publications;


namespace Davydova_Task4_Library.DAL.Interfaces
{
    public interface IBaseDao
    {
        int AddLibraryObject(LibraryObject libraryObject);
        void UpdateLibraryObject(LibraryObject libraryObject);
        bool DeleteLibraryObject(int idLibraryObject);
        IEnumerable<LibraryObject> GetAllLibraryObjects();
        IEnumerable<LibraryObject>  GetLibraryObjectByName(string nameLibraryObject);
        IEnumerable<LibraryObject> SortByPublicationYear();
        IEnumerable<LibraryObject> ReverseSortByPublicationYear();
        IEnumerable<Book> GetBooksByAuthorId(int authorId);
        IEnumerable<Patent> GetPatentsByInventorId(int inventorId);
        IEnumerable<LibraryObject> GetBooksAndPatentsByAuthorId(int authorId);
        IEnumerable<IEnumerable<Book>> GetBooksStartByChars(string publishingHouse);
        IEnumerable<IEnumerable<LibraryObject>> GroupByPublicationYear();
    }
}
