﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Davydova_Task4_Library.Library;

namespace Davydova_Task4_Library.DAL_Interfaces
{
    public interface ILibraryObject
    {
        LibraryObject GetLibraryObjectById(int idLibraryObject);

        LibraryObject GetLibraryObjectByName(string name);

        int AddLibraryObject(LibraryObject libraryObject);

        bool DeleteLibraryObject(int libraryObjectId);

        IEnumerable<LibraryObject> GetAllLibraryObjects();

        void UpdateLibraryObject(LibraryObject libraryObject);
    }
}
