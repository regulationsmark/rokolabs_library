﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.Entities.LinkedDto
{
    public class PatentsByInventors
    {
        public int PatentId { get; set; }
        public int InventorId { get; set; }

        public PatentsByInventors(int patentId, int inventorId)
        {
            PatentId = patentId;
            InventorId = inventorId;
        }
    }
}
