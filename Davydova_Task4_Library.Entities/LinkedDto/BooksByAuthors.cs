﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.Entities.LinkedDto
{
    public class BooksByAuthors
    {
        public int BookId { get; set; }
        public int AuthorId { get; set; }

        public BooksByAuthors(int bookId, int authorId)
        {
            BookId = bookId;
            AuthorId = authorId;
        }
    }
}
