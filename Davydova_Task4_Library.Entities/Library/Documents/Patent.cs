﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Davydova_Task4_Library.Library.OtherEntities;

namespace Davydova_Task4_Library.Library.Documents
{
    public class Patent:LibraryObject
    {
        public List<Author> Inventors { get; set; }

        public string Country { get; set; }

        public int RegistrationNumber { get; set; }
        public DateTime ApplicationDate { get; set; }

        public DateTime PublicationDate { get; set; }

        public Patent(int id, string name, int countOfPages, string note, List<Author> inventors, int registrationNumber, DateTime applicationDate, DateTime publicationDate, string country) 
            :base(id, name, applicationDate.Year, countOfPages, note)
        {
            Inventors = inventors;
            Country = country;
            RegistrationNumber = registrationNumber;
            ApplicationDate = applicationDate;
            PublicationDate = publicationDate;
        }

        public Patent(string name, int countOfPages, string note, List<Author> inventors, int registrationNumber, DateTime applicationDate, DateTime publicationDate, string country)
            : base(name, applicationDate.Year, countOfPages, note)
        {
            Inventors = inventors;
            Country = country;
            RegistrationNumber = registrationNumber;
            ApplicationDate = applicationDate;
            PublicationDate = publicationDate;
        }
        public override bool Equals(object obj)
        {
            if (obj is Patent patent)
            {
                return this.RegistrationNumber == patent.RegistrationNumber && this.Country == patent.Country;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return $"Name: {Name}" + Environment.NewLine +
                $"Inventors: {Inventors}" + Environment.NewLine +
                $"Publication Date: {PublicationDate} " + $"Date Of Application: {ApplicationDate} ";
        }
    }
}
