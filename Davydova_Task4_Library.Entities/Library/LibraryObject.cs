﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library
{
    public class LibraryObject:IComparable<LibraryObject>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int PublicationYear { get; set; }
        public int CountOfPages { get; set; }
        public string Note { get; set; }

        public LibraryObject(int id, string name, int publicationYear, int countOfPages, string note)
        {
            Id = id;
            Name = name;
            PublicationYear = publicationYear;
            CountOfPages = countOfPages;
            Note = note;
        }

        public LibraryObject(string name, int publicationYear, int countOfPages, string note)
        {
            Name = name;
            PublicationYear = publicationYear;
            CountOfPages = countOfPages;
            Note = note;
        }
        public int CompareTo(LibraryObject libraryObject)
        {
            if (libraryObject == null)
                return 1;

            else
                return this.PublicationYear.CompareTo(libraryObject.PublicationYear);

        }

        public override string ToString()
        {
            return $"Name:{Name}" + Environment.NewLine +
                $"Publication year {PublicationYear} + Count of pages: { CountOfPages}" +  Environment.NewLine +
                $"Note:{Note}";
        }
    }
}
