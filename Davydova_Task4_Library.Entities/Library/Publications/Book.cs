﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Davydova_Task4_Library.Library.OtherEntities;

namespace Davydova_Task4_Library.Library.Publications
{
    public class Book : Publication
    {
        public List<Author> Authors { get; set; }
        public string ISBN { get; set; }

        public Book(int id, string name, int publicationYear, int countOfPages, string note, string placeOfPublication, string publishingHouse, List<Author> authors, string iSBN) 
            : base(id, name, publicationYear, countOfPages, note, placeOfPublication, publishingHouse)
        {
            Authors = authors;
            ISBN = iSBN;
        }

        public Book(string name, int publicationYear, int countOfPages, string note, string placeOfPublication, string publishingHouse, List<Author> authors, string iSBN)
            : base(name, publicationYear, countOfPages, note, placeOfPublication, publishingHouse)
        {
            Authors = authors;
            ISBN = iSBN;
        }

        public override bool Equals(object obj)
        {
            if (obj is Book book)
            {
                if (!string.IsNullOrWhiteSpace(this.ISBN) && !string.IsNullOrWhiteSpace(book.ISBN))
                {
                    return book.ISBN == this.ISBN;
                }
                return this.Name == book.Name && this.Authors.Equals(book.Authors) && this.PublicationYear == book.PublicationYear;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return $"Name:{Name}" + Environment.NewLine +
                $"Authors {Authors}" + Environment.NewLine +
                $"Place of publication: {PlaceOfPublication} Publishing House:{PublishingHouse}" + Environment.NewLine +
                $"Year of publication{PublicationYear}";
        }

    }
}
