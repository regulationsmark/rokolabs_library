﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.Library.Publications
{
    public class Publication:LibraryObject
    {
        public string PublishingHouse { get; set; }
        public string PlaceOfPublication { get; set; }

        public Publication(int id, string name, int publicationYear, int countOfPages, string note, string placeOfPublication, string publishingHouse)
            :base(id, name, publicationYear, countOfPages, note)
        {
            PublishingHouse = publishingHouse;

            PlaceOfPublication = placeOfPublication;
        }

        public Publication(string name, int publicationYear, int countOfPages, string note, string placeOfPublication, string publishingHouse)
            : base(name, publicationYear, countOfPages, note)
        {
            PublishingHouse = publishingHouse;

            PlaceOfPublication = placeOfPublication;
        }
        public override string ToString()
        {
            return $"Publishing house:{PublishingHouse}" + Environment.NewLine +
                $"Place of publication {PlaceOfPublication}";
        }
    }
}
