﻿using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.DAL_Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

namespace Davydova_Task4_Library.RealDAL
{
    public static class DaoConfigurator
    {
        /*public static IConfigurationRoot BuildConfiguration()
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .Build();
            return configuration;
        }*/

        public static IServiceCollection AddAuthorRealDao(this IServiceCollection services)
        {
            return services.AddTransient<IAuthorDao, AuthorDao>();
        }

        public static IServiceCollection AddBookRealDao(this IServiceCollection services)
        {
            return services.AddTransient<IBookDao, BookDao>();
        }

        public static IServiceCollection AddBaseRealDao(this IServiceCollection services)
        {
            return services.AddTransient<IBaseDao, BaseDao>();
        }

        public static IServiceCollection AddLibraryObjectRealDao(this IServiceCollection services)
        {
            return services.AddTransient<ILibraryObject, LibraryObjectDao>();
        }
        public static IServiceCollection AddPublicationRealDao(this IServiceCollection services)
        {
            return services.AddTransient<IPublicationDao, PublicationDao>();
        }

        public static IServiceCollection AddNewspaperRealDao(this IServiceCollection services)
        {
            return services.AddTransient<INewspaperDao, NewspaperDao>();
        }

        public static IServiceCollection AddPatentRealDao(this IServiceCollection services)
        {
            return services.AddTransient<IPatentDao, PatentDao>();
        }
    }
}
