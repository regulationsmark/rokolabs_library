﻿using Davydova_Task4_Library.DAL_Interfaces;
using Davydova_Task4_Library.Library.Publications;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.RealDAL
{
    public class NewspaperDao : SqlDbCommandHelper, INewspaperDao
    {
        private IConfigurationRoot _configuration;
        public NewspaperDao(IConfigurationRoot configuration)
        {
            _configuration = configuration;
        }
        public int AddNewsPaper(NewsPaper newspaper)
        {
            var newspaperId = 0;
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "AddNewspaper");
                AddParameter(command, CreateParameter("@Name", DbType.String, newspaper.Name));
                AddParameter(command, CreateParameter("@PublicationYear", DbType.Int32, newspaper.PublicationYear));
                AddParameter(command, CreateParameter("@CountOfPages", DbType.Int32, newspaper.CountOfPages));
                AddParameter(command, CreateParameter("@Note", DbType.String, newspaper.Note));
                AddParameter(command, CreateParameter("@PublishingHouse", DbType.String, newspaper.PublishingHouse));
                AddParameter(command, CreateParameter("@PlaceOfPublication", DbType.String, newspaper.PlaceOfPublication));
                AddParameter(command, CreateParameter("@IssueNumber", DbType.Int32, newspaper.IssueNumber));
                AddParameter(command, CreateParameter("@IssueDate", DbType.DateTime, newspaper.IssueDate));
                AddParameter(command, CreateParameter("@ISBN", DbType.String, newspaper.ISSN));
                connection.Open();
                newspaperId = (int)command.ExecuteScalar();
                //Console.WriteLine(newspaperId);
            }
            return newspaperId;
        }

        public bool DeleteNewsPaper(int newspaperId)
        {
            var result = 0;
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "DeleteNewspaperById");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, newspaperId));
                connection.Open();
                result = command.ExecuteNonQuery();
            }
            if (result == -1)
            {
                return false;
            }
            return true;
        }

        public IEnumerable<NewsPaper> GetAllNewspapers()
        {
            List<NewsPaper> newspapers = new List<NewsPaper>();

            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetAllNewspapers");
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = (int)reader["Id"];
                        var name = reader["Name"].ToString();
                        var publicationYear = (int)reader["PublicationYear"];
                        var countOfPages = (int)reader["CountOfPages"];
                        var note = reader["Note"].ToString();
                        var publishingHouse = reader["PublishingHouse"].ToString();
                        var placeOfPublication = reader["PlaceOfPublication"].ToString();
                        var issueDate = (DateTime)reader["IssueDate"];
                        var issueNumber = (int)reader["IssueNumber"];
                        var iSSN = reader["ISSN"].ToString();
                        NewsPaper newspaper = new NewsPaper(id, name, publicationYear, countOfPages, note, publishingHouse, placeOfPublication, issueNumber, issueDate, iSSN);
                        newspapers.Add(newspaper);
                    }

                }
            }

            return newspapers;
        }

        public NewsPaper GetNewpaperById(int newspaperId)
        {
            NewsPaper newspaper = null;
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetNewpaperById");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, newspaperId));
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = (int)reader["Id"];
                        var name = reader["Name"].ToString();
                        var publicationYear = (int)reader["PublicationYear"];
                        var countOfPages = (int)reader["CountOfPages"];
                        var note = reader["Note"].ToString();
                        var publishingHouse = reader["PublishingHouse"].ToString();
                        var placeOfPublication = reader["PlaceOfPublication"].ToString();
                        var issueDate = (DateTime)reader["IssueDate"];
                        var issueNumber = (int)reader["IssueNumber"];
                        var iSSN = reader["ISSN"].ToString();
                        newspaper = new NewsPaper(id, name, publicationYear, countOfPages, note, publishingHouse, placeOfPublication, issueNumber, issueDate, iSSN);
                    }

                }
            }

            return newspaper;
        }

        public void UpdateNewspaper(NewsPaper newspaper)
        {
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "UpdateNewspaper");
                AddParameter(command, CreateParameter("@Name", DbType.String, newspaper.Name));
                AddParameter(command, CreateParameter("@PublicationYear", DbType.Int32, newspaper.PublicationYear));
                AddParameter(command, CreateParameter("@CountOfPages", DbType.Int32, newspaper.CountOfPages));
                AddParameter(command, CreateParameter("@Note", DbType.String, newspaper.Note));
                AddParameter(command, CreateParameter("@PublishingHouse", DbType.String, newspaper.PublishingHouse));
                AddParameter(command, CreateParameter("@PlaceOfPublication", DbType.String, newspaper.PlaceOfPublication));
                AddParameter(command, CreateParameter("@IssueNumber", DbType.Int32, newspaper.IssueNumber));
                AddParameter(command, CreateParameter("@IssueDate", DbType.DateTime, newspaper.IssueDate));
                AddParameter(command, CreateParameter("@ISBN", DbType.String, newspaper.ISSN));
                connection.Open();
                var newspaperId = command.ExecuteNonQuery();
                //Console.WriteLine(newspaperId);
            }
        }
    }
}
