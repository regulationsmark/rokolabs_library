﻿using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.DAL_Interfaces;
using Davydova_Task4_Library.Entities.LinkedDto;
using Davydova_Task4_Library.Library.Documents;
using Davydova_Task4_Library.Library.OtherEntities;
using Davydova_Task4_Library.Library.Publications;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.RealDAL
{
    public class BaseDao : SqlDbCommandHelper, IBaseDao
    {
        private IConfigurationRoot _configuration;
        private IBookDao _bookDao;
        private INewspaperDao _newspaperDao;
        private IPatentDao _patentDao;
        private IAuthorDao _authorDao;
        public BaseDao(IConfigurationRoot configuration, IAuthorDao authorDao, IBookDao bookDao, INewspaperDao newspaperDao, IPatentDao patentDao)
        {
            _authorDao = authorDao;
            _bookDao = bookDao;
            _newspaperDao = newspaperDao;
            _patentDao = patentDao;
            _configuration = configuration;
        }

        public int AddLibraryObject(LibraryObject libraryObject)
        {
            var libraryObjectId = 0;

            if (libraryObject is Book)
            {
                List<int> authorsIds = new List<int>();
                foreach (Author author in ((Book)libraryObject).Authors)
                {
                    authorsIds.Add(author.Id);
                }
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = CreateCommand(connection, "AddBook");
                    AddParameter(command, CreateParameter("@Name", DbType.String, libraryObject.Name));
                    AddParameter(command, CreateParameter("@PublicationYear", DbType.Int32, libraryObject.PublicationYear));
                    AddParameter(command, CreateParameter("@CountOfPages", DbType.Int32, libraryObject.CountOfPages));
                    AddParameter(command, CreateParameter("@Note", DbType.String, libraryObject.Note));
                    AddParameter(command, CreateParameter("@PublishingHouse", DbType.String, ((Publication)libraryObject).PublishingHouse));
                    AddParameter(command, CreateParameter("@PlaceOfPublication", DbType.String, ((Publication)libraryObject).PlaceOfPublication));
                    AddParameter(command, CreateParameter("@ISBN", DbType.String, ((Book)libraryObject).ISBN));
                    AddParameter(command, CreateParameter("@AuthorsIds", DbType.Object, authorsIds));
                    connection.Open();
                    libraryObjectId = (int)command.ExecuteScalar();
                }
            }
            else if (libraryObject is NewsPaper)
            {
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = CreateCommand(connection, "AddNewspaper");
                    AddParameter(command, CreateParameter("@Name", DbType.String, libraryObject.Name));
                    AddParameter(command, CreateParameter("@PublicationYear", DbType.Int32, libraryObject.PublicationYear));
                    AddParameter(command, CreateParameter("@CountOfPages", DbType.Int32, libraryObject.CountOfPages));
                    AddParameter(command, CreateParameter("@Note", DbType.String, libraryObject.Note));
                    AddParameter(command, CreateParameter("@PublishingHouse", DbType.String, ((Publication)libraryObject).PublishingHouse));
                    AddParameter(command, CreateParameter("@PlaceOfPublication", DbType.String, ((Publication)libraryObject).PlaceOfPublication));
                    AddParameter(command, CreateParameter("@IssueNumber", DbType.Int32, ((NewsPaper)libraryObject).IssueNumber));
                    AddParameter(command, CreateParameter("@IssueDate", DbType.DateTime, ((NewsPaper)libraryObject).IssueDate));
                    AddParameter(command, CreateParameter("@ISSN", DbType.String, ((NewsPaper)libraryObject).ISSN));
                    connection.Open();
                    libraryObjectId = (int)command.ExecuteScalar();
                }
            }
            else if (libraryObject is Patent)
            {
                List<int> inventorsIds = new List<int>();
                foreach (Author inventor in ((Patent)libraryObject).Inventors)
                {
                    inventorsIds.Add(inventor.Id);
                }
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = CreateCommand(connection, "AddPatent");
                    AddParameter(command, CreateParameter("@Name", DbType.String, libraryObject.Name));
                    AddParameter(command, CreateParameter("@PublicationYear", DbType.Int32, libraryObject.PublicationYear));
                    AddParameter(command, CreateParameter("@CountOfPages", DbType.Int32, libraryObject.CountOfPages));
                    AddParameter(command, CreateParameter("@Note", DbType.String, libraryObject.Note));
                    AddParameter(command, CreateParameter("@Country", DbType.String, ((Patent)libraryObject).Country));
                    AddParameter(command, CreateParameter("@RegistrationNumber", DbType.Int32, ((Patent)libraryObject).RegistrationNumber));
                    AddParameter(command, CreateParameter("@ApplicationDate", DbType.DateTime, ((Patent)libraryObject).ApplicationDate));
                    AddParameter(command, CreateParameter("@PublicationDate", DbType.DateTime, ((Patent)libraryObject).PublicationDate));
                    AddParameter(command, CreateParameter("@InventorsIds", DbType.Object, inventorsIds));
                    connection.Open();
                    libraryObjectId = (int)command.ExecuteScalar();
                }
            }
            return libraryObjectId;
        }

        public bool DeleteLibraryObject(int idLibraryObject)
        {
            var result = 0;
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "DeleteLibraryObjectById");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, idLibraryObject));
                connection.Open();
                result = command.ExecuteNonQuery();
            }
            if (result == -1)
            {
                return false;
            }
            return true;
        }

        public void UpdateLibraryObject(LibraryObject libraryObject)
        {
            var result = 0;
            if (libraryObject is Book)
            {
                List<int> authorsIds = new List<int>();
                foreach (Author author in ((Book)libraryObject).Authors)
                {
                    authorsIds.Add(author.Id);
                }
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = CreateCommand(connection, "UpdateBook");
                    AddParameter(command, CreateParameter("@Name", DbType.String, libraryObject.Name));
                    AddParameter(command, CreateParameter("@PublicationYear", DbType.Int32, libraryObject.PublicationYear));
                    AddParameter(command, CreateParameter("@CountOfPages", DbType.Int32, libraryObject.CountOfPages));
                    AddParameter(command, CreateParameter("@Note", DbType.String, libraryObject.Note));
                    AddParameter(command, CreateParameter("@PublishingHouse", DbType.String, ((Publication)libraryObject).PublishingHouse));
                    AddParameter(command, CreateParameter("@PlaceOfPublication", DbType.String, ((Publication)libraryObject).PlaceOfPublication));
                    AddParameter(command, CreateParameter("@ISBN", DbType.String, ((Book)libraryObject).ISBN));
                    AddParameter(command, CreateParameter("@AuthorsIds", DbType.Object, authorsIds));
                    connection.Open();
                    result = (int)command.ExecuteNonQuery();
                    //Console.WriteLine(result);
                }
            }
            else if (libraryObject is NewsPaper)
            {
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = CreateCommand(connection, "UpdateNewspaper");
                    AddParameter(command, CreateParameter("@Name", DbType.String, libraryObject.Name));
                    AddParameter(command, CreateParameter("@PublicationYear", DbType.Int32, libraryObject.PublicationYear));
                    AddParameter(command, CreateParameter("@CountOfPages", DbType.Int32, libraryObject.CountOfPages));
                    AddParameter(command, CreateParameter("@Note", DbType.String, libraryObject.Note));
                    AddParameter(command, CreateParameter("@PublishingHouse", DbType.String, ((Publication)libraryObject).PublishingHouse));
                    AddParameter(command, CreateParameter("@PlaceOfPublication", DbType.String, ((Publication)libraryObject).PlaceOfPublication));
                    AddParameter(command, CreateParameter("@IssueNumber", DbType.Int32, ((NewsPaper)libraryObject).IssueNumber));
                    AddParameter(command, CreateParameter("@IssueDate", DbType.DateTime, ((NewsPaper)libraryObject).IssueDate));
                    AddParameter(command, CreateParameter("@ISSN", DbType.String, ((NewsPaper)libraryObject).ISSN));
                    connection.Open();
                    result = (int)command.ExecuteScalar();
                }
            }
            else if (libraryObject is Patent)
            {
                List<int> inventorsIds = new List<int>();
                foreach (Author inventor in ((Patent)libraryObject).Inventors)
                {
                    inventorsIds.Add(inventor.Id);
                }
                using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    var command = CreateCommand(connection, "UpdatePatent");
                    AddParameter(command, CreateParameter("@Name", DbType.String, libraryObject.Name));
                    AddParameter(command, CreateParameter("@PublicationYear", DbType.Int32, libraryObject.PublicationYear));
                    AddParameter(command, CreateParameter("@CountOfPages", DbType.Int32, libraryObject.CountOfPages));
                    AddParameter(command, CreateParameter("@Note", DbType.String, libraryObject.Note));
                    AddParameter(command, CreateParameter("@Country", DbType.String, ((Patent)libraryObject).Country));
                    AddParameter(command, CreateParameter("@RegistrationNumber", DbType.Int32, ((Patent)libraryObject).RegistrationNumber));
                    AddParameter(command, CreateParameter("@ApplicationDate", DbType.DateTime, ((Patent)libraryObject).ApplicationDate));
                    AddParameter(command, CreateParameter("@PublicationDate", DbType.DateTime, ((Patent)libraryObject).PublicationDate));
                    AddParameter(command, CreateParameter("@InventorsIds", DbType.Object, inventorsIds));
                    connection.Open();
                    result = (int)command.ExecuteScalar();
                }
            }
        }

        public IEnumerable<LibraryObject> GetAllLibraryObjects()
        {
            List<LibraryObject> libraryObjects = null;
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetTypeOfLibraryObject");
                connection.Open();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var typeOfLibraryObject = reader["TypeLibraryObject"].ToString();
                    switch (typeOfLibraryObject)
                    {
                        case "Book":
                            foreach (Book book in _bookDao.GetAllBooks())
                            {
                                libraryObjects.Add(book);
                            }
                            break;
                        case "Patent":
                            foreach (Patent patent in _patentDao.GetAllPatents())
                            {
                                libraryObjects.Add(patent);
                            }
                            break;
                        case "Newspaper":
                            foreach (NewsPaper newspaper in _newspaperDao.GetAllNewspapers())
                            {
                                libraryObjects.Add(newspaper);
                            }
                            break;
                    }
                }
            }
            return libraryObjects;
        }

        public IEnumerable<LibraryObject> GetBooksAndPatentsByAuthorId(int authorId)
        {
            var libraryObjects = new List<LibraryObject>();

            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetPatentsAndBooksByAuthorId");
                AddParameter(command, CreateParameter("@Id", DbType.String, authorId));
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var typeOfLibraryObject = reader["TypeLibraryObject"].ToString();
                        switch (typeOfLibraryObject)
                        {
                            case "Book":
                                foreach (Book book in _bookDao.GetBooksByAuthorId(authorId))
                                {
                                    libraryObjects.Add(book);
                                }
                                break;
                            case "Patent":
                                foreach (Patent patent in _patentDao.GetPatentsByInventorId(authorId))
                                {
                                    libraryObjects.Add(patent);
                                }
                                break;
                        }
                    }
                }
            }
            return libraryObjects;
        }

        public IEnumerable<Book> GetBooksByAuthorId(int authorId)
        {
            var books = new List<Book>();
            List<Author> authors = new List<Author>();
            Author author = null;

            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetBooksByAuthorId");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, authorId));
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = (int)reader["Id"];
                        var name = reader["Name"].ToString();
                        var publicationYear = (int)reader["PublicationYear"];
                        var countOfPages = (int)reader["CountOfPages"];
                        var note = reader["Note"].ToString();
                        var publishingHouse = reader["PublishingHouse"].ToString();
                        var placeOfPublication = reader["PlaceOfPublication"].ToString();
                        var iSBN = reader["ISBN"].ToString();
                        Book book = new Book(id, name, publicationYear, countOfPages, note, placeOfPublication, publishingHouse, authors, iSBN);
                        books.Add(book);
                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        var id = (int)reader["authorId"];
                        var name = reader["authorName"].ToString();
                        var surname = reader["authorSurname"].ToString();
                        author = new Author(id, name, surname);
                    }
                }
                foreach (var book in books)
                {
                    book.Authors.Add(author);

                }
            }
            return books;
        }

        public IEnumerable<IEnumerable<Book>> GetBooksStartByChars(string publishingHouse)
        {
            List<Book> books = new List<Book>();
            List<Author> authors = new List<Author>();
            List<BooksByAuthors> booksByAuthors = new List<BooksByAuthors>();

            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetBooksByCharacterSetPublishingHouse");
                AddParameter(command, CreateParameter("@CharacterSetPublishingHouse", DbType.String, publishingHouse));
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = (int)reader["Id"];
                        var name = reader["Name"].ToString();
                        var publicationYear = (int)reader["PublicationYear"];
                        var countOfPages = (int)reader["CountOfPages"];
                        var note = reader["Note"].ToString();
                        var bookpublishingHouse = reader["PublishingHouse"].ToString();
                        var placeOfPublication = reader["PlaceOfPublication"].ToString();
                        var iSBN = reader["ISBN"].ToString();
                        Book book = new Book(id, name, publicationYear, countOfPages, note, placeOfPublication, bookpublishingHouse, authors, iSBN);
                        books.Add(book);
                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        var authorId = (int)reader["IdAuthor"];
                        var bookId = (int)reader["IdBook"];
                        BooksByAuthors booksByAuthorsItem = new BooksByAuthors(bookId, authorId);
                        booksByAuthors.Add(booksByAuthorsItem);
                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        var id = (int)reader["authorId"];
                        var name = reader["authorName"].ToString();
                        var surname = reader["authorSurname"].ToString();
                        Author author = new Author(id, name, surname);
                        authors.Add(author);
                    }
                }
            }
            foreach (var book in books)
            {
                var booksByAuthorsInfos = booksByAuthors.Where(p => p.BookId == book.Id);
                foreach (var booksByAuthorsInfo in booksByAuthorsInfos)
                {
                    var author = authors.First(p => p.Id == booksByAuthorsInfo.AuthorId);
                    Author newAuthor = new Author(author.Id, author.Name, author.Surname);
                    book.Authors.Add(newAuthor);
                }
            }
            return books.GroupBy(book => book.PublishingHouse);
        }

        public IEnumerable<LibraryObject> GetLibraryObjectByName(string nameLibraryObject)
        {
            var libraryObjects = new List<LibraryObject>();
            List<Book> books = new List<Book>();
            List<Author> authors = new List<Author>();
            List<BooksByAuthors> booksByAuthors = new List<BooksByAuthors>();
            List<Patent> patents = new List<Patent>();
            List<Author> inventors = new List<Author>();
            List<PatentsByInventors> patentsByInventors = new List<PatentsByInventors>();
            List<NewsPaper> newspapers = new List<NewsPaper>();

            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetLibraryObjectsBySearchingString");
                AddParameter(command, CreateParameter("@SearchingString", DbType.String, nameLibraryObject));
                connection.Open(); 
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        switch (reader["TypeLibraryObject"])
                        {
                            case "Book":
                                var idBook = (int)reader["Id"];
                                var nameBook = reader["Name"].ToString();
                                var publicationYearBook = (int)reader["PublicationYear"];
                                var countOfPagesBook = (int)reader["CountOfPages"];
                                var noteBook = reader["Note"].ToString();
                                var publishingHouseBook = reader["PublishingHouse"].ToString();
                                var placeOfPublicationBook = reader["PlaceOfPublication"].ToString();
                                var iSBN = reader["ISBN"].ToString();
                                Book book = new Book(idBook, nameBook, publicationYearBook, countOfPagesBook, noteBook, publishingHouseBook, placeOfPublicationBook, authors, iSBN);
                                books.Add(book);
                                break;
                            case "Patent":
                                var idPatent = (int)reader["Id"];
                                var namePatent = reader["Name"].ToString();
                                var countOfPagesPatent = (int)reader["CountOfPages"];
                                var notePatent = reader["Note"].ToString();
                                var country = reader["Country"].ToString();
                                var registrationNumber = (int)reader["RegistrationNumber"];
                                var applicationDate = (DateTime)reader["ApplicationDate"];
                                var publicationDate = (DateTime)reader["PublicationDate"];
                                Patent patent = new Patent(idPatent, namePatent, countOfPagesPatent, notePatent, inventors, registrationNumber, applicationDate, publicationDate, country);
                                patents.Add(patent);
                                break;
                            case "Newspaper":
                                var idNewspaper = (int)reader["Id"];
                                var nameNespaper = reader["Name"].ToString();
                                var publicationYearNewspaper = (int)reader["PublicationYear"];
                                var countOfPagesNewspaper = (int)reader["CountOfPages"];
                                var noteNewspaper = reader["Note"].ToString();
                                var publishingHouseNewspaper = reader["PublishingHouse"].ToString();
                                var placeOfPublicationNewspaper = reader["PlaceOfPublication"].ToString();
                                var issueDate = (DateTime)reader["IssueDate"];
                                var issueNumber = (int)reader["IssueNumber"];
                                var iSSN = reader["ISSN"].ToString();
                                NewsPaper newspaper = new NewsPaper(idNewspaper, nameNespaper, publicationYearNewspaper, countOfPagesNewspaper, noteNewspaper, publishingHouseNewspaper, placeOfPublicationNewspaper, issueNumber, issueDate, iSSN);
                                newspapers.Add(newspaper);
                                break;
                        }

                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        var authorId = (int)reader["IdAuthor"];
                        var bookId = (int)reader["IdBook"];
                        BooksByAuthors booksByAuthorsItem = new BooksByAuthors(bookId, authorId);
                        booksByAuthors.Add(booksByAuthorsItem);
                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        var inventorId = (int)reader["IDInventor"];
                        var patentId = (int)reader["IdPatent"];
                        PatentsByInventors patentsByInventorsItem = new PatentsByInventors(patentId, inventorId);
                        patentsByInventors.Add(patentsByInventorsItem);
                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        var id = (int)reader["authorId"];
                        var name = reader["authorName"].ToString();
                        var surname = reader["authorSurname"].ToString();
                        Author author = new Author(id, name, surname);
                        authors.Add(author);
                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        var id = (int)reader["authorId"];
                        var name = reader["authorName"].ToString();
                        var surname = reader["authorSurname"].ToString();
                        Author inventor = new Author(id, name, surname);
                        inventors.Add(inventor);
                    }
                }
            }
            foreach (var book in books)
            {
                var booksByAuthorsInfos = booksByAuthors.Where(p => p.BookId == book.Id);
                foreach (var booksByAuthorsInfo in booksByAuthorsInfos)
                {
                    var author = authors.First(p => p.Id == booksByAuthorsInfo.AuthorId);
                    Author newAuthor = new Author(author.Id, author.Name, author.Surname);
                    book.Authors.Add(newAuthor);
                }
                libraryObjects.Add(book);
            }

            foreach (var patent in patents)
            {
                var patentsByInventorsInfos = patentsByInventors.Where(p => p.PatentId == patent.Id);
                foreach (var patentsByAuthorsInfo in patentsByInventorsInfos)
                {
                    var inventor = inventors.First(p => p.Id == patentsByAuthorsInfo.InventorId);
                    Author newInventor = new Author(inventor.Id, inventor.Name, inventor.Surname);
                    patent.Inventors.Add(newInventor);
                }
                libraryObjects.Add(patent);
            }

            foreach (var newspaper in newspapers)
            {
                libraryObjects.Add(newspaper);
            }

            return libraryObjects;
        }

        public IEnumerable<Patent> GetPatentsByInventorId(int inventorId)
        {
            var patents = new List<Patent>();
            List<Author> inventors = new List<Author>();
            Author inventor = null;

            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetPatentsByInventorId");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, inventorId));
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = (int)reader["Id"];
                        var name = reader["Name"].ToString();
                        var publicationYear = (int)reader["PublicationYear"];
                        var countOfPages = (int)reader["CountOfPages"];
                        var note = reader["Note"].ToString();
                        var country = reader["Country"].ToString();
                        var registrationNumber = (int)reader["RegistrationNumber"];
                        var applicationDate = (DateTime)reader["ApplicationDate"];
                        var publicationDate = (DateTime)reader["PublicationDate"];
                        Patent patent = new Patent(id, name, countOfPages, note, inventors, registrationNumber, applicationDate, publicationDate, country);
                        patents.Add(patent);
                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        var id = (int)reader["authorId"];
                        var name = reader["authorName"].ToString();
                        var surname = reader["authorSurname"].ToString();
                        inventor = new Author(id, name, surname);
                    }
                }
                foreach (var patent in patents)
                {
                    patent.Inventors.Add(inventor);

                }
            }
            return patents;
        }

        public IEnumerable<IEnumerable<LibraryObject>> GroupByPublicationYear()
        {
            List<LibraryObject> libraryObjects = null;
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetTypeOfLibraryObject");
                connection.Open();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var typeOfLibraryObject = reader["TypeLibraryObject"].ToString();
                    switch (typeOfLibraryObject)
                    {
                        case "Book":
                            foreach(Book book in _bookDao.GetAllBooks())
                            {
                                libraryObjects.Add(book);
                            }
                            break;
                        case "Patent":
                            foreach(Patent patent in _patentDao.GetAllPatents())
                            {
                                libraryObjects.Add(patent);
                            }
                            break;
                        case "Newspaper":
                            foreach(NewsPaper newspaper in _newspaperDao.GetAllNewspapers())
                            {
                                libraryObjects.Add(newspaper);
                            }
                            break;
                    }
                }
            }
            return libraryObjects.GroupBy(obj => obj.PublicationYear);
        }

        public IEnumerable<LibraryObject> ReverseSortByPublicationYear()
        {
            var libraryObjects = new List<LibraryObject>();
            List<Book> books = new List<Book>();
            List<Author> authors = new List<Author>();
            List<BooksByAuthors> booksByAuthors = new List<BooksByAuthors>();
            List<Patent> patents = new List<Patent>();
            List<Author> inventors = new List<Author>();
            List<PatentsByInventors> patentsByInventors = new List<PatentsByInventors>();
            List<NewsPaper> newspapers = new List<NewsPaper>();

            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetReverseSortedBookByPublicationYear");
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        switch (reader["TypeLibraryObject"])
                        {
                            case "Book":
                                var idBook = (int)reader["Id"];
                                var nameBook = reader["Name"].ToString();
                                var publicationYearBook = (int)reader["PublicationYear"];
                                var countOfPagesBook = (int)reader["CountOfPages"];
                                var noteBook = reader["Note"].ToString();
                                var publishingHouseBook = reader["PublishingHouse"].ToString();
                                var placeOfPublicationBook = reader["PlaceOfPublication"].ToString();
                                var iSBN = reader["ISBN"].ToString();
                                Book book = new Book(idBook, nameBook, publicationYearBook, countOfPagesBook, noteBook, publishingHouseBook, placeOfPublicationBook, authors, iSBN);
                                books.Add(book);
                                break;
                            case "Patent":
                                var idPatent = (int)reader["Id"];
                                var namePatent = reader["Name"].ToString();
                                var countOfPagesPatent = (int)reader["CountOfPages"];
                                var notePatent = reader["Note"].ToString();
                                var country = reader["Country"].ToString();
                                var registrationNumber = (int)reader["RegistrationNumber"];
                                var applicationDate = (DateTime)reader["ApplicationDate"];
                                var publicationDate = (DateTime)reader["PublicationDate"];
                                Patent patent = new Patent(idPatent, namePatent, countOfPagesPatent, notePatent, inventors, registrationNumber, applicationDate, publicationDate, country);
                                patents.Add(patent);
                                break;
                            case "Newspaper":
                                var idNewspaper = (int)reader["Id"];
                                var nameNespaper = reader["Name"].ToString();
                                var publicationYearNewspaper = (int)reader["PublicationYear"];
                                var countOfPagesNewspaper = (int)reader["CountOfPages"];
                                var noteNewspaper = reader["Note"].ToString();
                                var publishingHouseNewspaper = reader["PublishingHouse"].ToString();
                                var placeOfPublicationNewspaper = reader["PlaceOfPublication"].ToString();
                                var issueDate = (DateTime)reader["IssueDate"];
                                var issueNumber = (int)reader["IssueNumber"];
                                var iSSN = reader["ISSN"].ToString();
                                NewsPaper newspaper = new NewsPaper(idNewspaper, nameNespaper, publicationYearNewspaper, countOfPagesNewspaper, noteNewspaper, publishingHouseNewspaper, placeOfPublicationNewspaper, issueNumber, issueDate, iSSN);
                                newspapers.Add(newspaper);
                                break;
                        }

                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        var authorId = (int)reader["IdAuthor"];
                        var bookId = (int)reader["IdBook"];
                        BooksByAuthors booksByAuthorsItem = new BooksByAuthors(bookId, authorId);
                        booksByAuthors.Add(booksByAuthorsItem);
                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        var inventorId = (int)reader["IDInventor"];
                        var patentId = (int)reader["IdPatent"];
                        PatentsByInventors patentsByInventorsItem = new PatentsByInventors(patentId, inventorId);
                        patentsByInventors.Add(patentsByInventorsItem);
                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        var id = (int)reader["authorId"];
                        var name = reader["authorName"].ToString();
                        var surname = reader["authorSurname"].ToString();
                        Author author = new Author(id, name, surname);
                        authors.Add(author);
                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        var id = (int)reader["authorId"];
                        var name = reader["authorName"].ToString();
                        var surname = reader["authorSurname"].ToString();
                        Author inventor = new Author(id, name, surname);
                        inventors.Add(inventor);
                    }
                }
            }
            foreach (var book in books)
            {
                var booksByAuthorsInfos = booksByAuthors.Where(p => p.BookId == book.Id);
                foreach (var booksByAuthorsInfo in booksByAuthorsInfos)
                {
                    var author = authors.First(p => p.Id == booksByAuthorsInfo.AuthorId);
                    Author newAuthor = new Author(author.Id, author.Name, author.Surname);
                    book.Authors.Add(newAuthor);
                }
                libraryObjects.Add(book);
            }

            foreach (var patent in patents)
            {
                var patentsByInventorsInfos = patentsByInventors.Where(p => p.PatentId == patent.Id);
                foreach (var patentsByAuthorsInfo in patentsByInventorsInfos)
                {
                    var inventor = inventors.First(p => p.Id == patentsByAuthorsInfo.InventorId);
                    Author newInventor = new Author(inventor.Id, inventor.Name, inventor.Surname);
                    patent.Inventors.Add(newInventor);
                }
                libraryObjects.Add(patent);
            }

            foreach (var newspaper in newspapers)
            {
                libraryObjects.Add(newspaper);
            }

            return libraryObjects;
        }

        public IEnumerable<LibraryObject> SortByPublicationYear()
        {
            var libraryObjects = new List<LibraryObject>();
            List<Book> books = new List<Book>();
            List<Author> authors = new List<Author>();
            List<BooksByAuthors> booksByAuthors = new List<BooksByAuthors>();
            List<Patent> patents = new List<Patent>();
            List<Author> inventors = new List<Author>();
            List<PatentsByInventors> patentsByInventors = new List<PatentsByInventors>();
            List<NewsPaper> newspapers = new List<NewsPaper>();

            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetSortedBookByPublicationYear");
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        switch (reader["TypeLibraryObject"])
                        {
                            case "Book":
                                var idBook = (int)reader["Id"];
                                var nameBook = reader["Name"].ToString();
                                var publicationYearBook = (int)reader["PublicationYear"];
                                var countOfPagesBook = (int)reader["CountOfPages"];
                                var noteBook = reader["Note"].ToString();
                                var publishingHouseBook = reader["PublishingHouse"].ToString();
                                var placeOfPublicationBook = reader["PlaceOfPublication"].ToString();
                                var iSBN = reader["ISBN"].ToString();
                                Book book = new Book(idBook, nameBook, publicationYearBook, countOfPagesBook, noteBook, publishingHouseBook, placeOfPublicationBook, authors, iSBN);
                                books.Add(book);
                                break;
                            case "Patent":
                                var idPatent = (int)reader["Id"];
                                var namePatent = reader["Name"].ToString();
                                var countOfPagesPatent = (int)reader["CountOfPages"];
                                var notePatent = reader["Note"].ToString();
                                var country = reader["Country"].ToString();
                                var registrationNumber = (int)reader["RegistrationNumber"];
                                var applicationDate = (DateTime)reader["ApplicationDate"];
                                var publicationDate = (DateTime)reader["PublicationDate"];
                                Patent patent = new Patent(idPatent, namePatent, countOfPagesPatent, notePatent, inventors, registrationNumber, applicationDate, publicationDate, country);
                                patents.Add(patent);
                                break;
                            case "Newspaper":
                                var idNewspaper = (int)reader["Id"];
                                var nameNespaper = reader["Name"].ToString();
                                var publicationYearNewspaper = (int)reader["PublicationYear"];
                                var countOfPagesNewspaper = (int)reader["CountOfPages"];
                                var noteNewspaper = reader["Note"].ToString();
                                var publishingHouseNewspaper = reader["PublishingHouse"].ToString();
                                var placeOfPublicationNewspaper = reader["PlaceOfPublication"].ToString();
                                var issueDate = (DateTime)reader["IssueDate"];
                                var issueNumber = (int)reader["IssueNumber"];
                                var iSSN = reader["ISSN"].ToString();
                                NewsPaper newspaper = new NewsPaper(idNewspaper, nameNespaper, publicationYearNewspaper, countOfPagesNewspaper, noteNewspaper, publishingHouseNewspaper, placeOfPublicationNewspaper, issueNumber, issueDate, iSSN);
                                newspapers.Add(newspaper);
                                break;
                        }
                    }
                    reader.NextResult();

                    while (reader.Read())
                    {
                        var authorId = (int)reader["IdAuthor"];
                        var bookId = (int)reader["IdBook"];
                        BooksByAuthors booksByAuthorsItem = new BooksByAuthors(bookId, authorId);
                        booksByAuthors.Add(booksByAuthorsItem);
                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        var inventorId = (int)reader["IDInventor"];
                        var patentId = (int)reader["IdPatent"];
                        PatentsByInventors patentsByInventorsItem = new PatentsByInventors(patentId, inventorId);
                        patentsByInventors.Add(patentsByInventorsItem);
                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        var id = (int)reader["authorId"];
                        var name = reader["authorName"].ToString();
                        var surname = reader["authorSurname"].ToString();
                        Author author = new Author(id, name, surname);
                        authors.Add(author);
                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        var id = (int)reader["authorId"];
                        var name = reader["authorName"].ToString();
                        var surname = reader["authorSurname"].ToString();
                        Author inventor = new Author(id, name, surname);
                        inventors.Add(inventor);
                    }
                }
            }
            foreach (var book in books)
            {
                var booksByAuthorsInfos = booksByAuthors.Where(p => p.BookId == book.Id);
                foreach (var booksByAuthorsInfo in booksByAuthorsInfos)
                {
                    var author = authors.First(p => p.Id == booksByAuthorsInfo.AuthorId);
                    Author newAuthor = new Author(author.Id, author.Name, author.Surname);
                    book.Authors.Add(newAuthor);
                }
                libraryObjects.Add(book);
            }

            foreach (var patent in patents)
            {
                var patentsByInventorsInfos = patentsByInventors.Where(p => p.PatentId == patent.Id);
                foreach (var patentsByAuthorsInfo in patentsByInventorsInfos)
                {
                    var inventor = inventors.First(p => p.Id == patentsByAuthorsInfo.InventorId);
                    Author newInventor = new Author(inventor.Id, inventor.Name, inventor.Surname);
                    patent.Inventors.Add(newInventor);
                }
                libraryObjects.Add(patent);
            }

            foreach (var newspaper in newspapers)
            {
                libraryObjects.Add(newspaper);
            }

            return libraryObjects;
        }
    }
}
