﻿using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.Entities.LinkedDto;
using Davydova_Task4_Library.Library.OtherEntities;
using Davydova_Task4_Library.Library.Publications;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.RealDAL
{
    public class BookDao : SqlDbCommandHelper, IBookDao
    {
        private IConfigurationRoot _configuration;
        private IAuthorDao _authorDao;
        public BookDao(IConfigurationRoot configuration, IAuthorDao authorDao)
        {
            _authorDao = authorDao;
            _configuration = configuration;
        }

        private static DataTable CreateDtListOfAuthors()
        {
            var dtListOfAuthors = new DataTable("ListOfAuthors");
            dtListOfAuthors.Columns.Add("AuthorId", typeof(int));
            return dtListOfAuthors;
        }
        public int AddBook(Book book)
        {
            var bookId = 0;
            DataTable dtListOfAuthors = CreateDtListOfAuthors();
            foreach (Author author in book.Authors)
            {
                DataRow row = dtListOfAuthors.NewRow();
                row["AuthorId"] = author.Id;
                dtListOfAuthors.Rows.Add(row);
            }
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "AddBook");
                AddParameter(command, CreateParameter("@Name", DbType.String, book.Name));
                AddParameter(command, CreateParameter("@PublicationYear", DbType.Int32, book.PublicationYear));
                AddParameter(command, CreateParameter("@CountOfPages", DbType.Int32, book.CountOfPages));
                AddParameter(command, CreateParameter("@Note", DbType.String, book.Note));
                AddParameter(command, CreateParameter("@PublishingHouse", DbType.String, book.PublishingHouse));
                AddParameter(command, CreateParameter("@PlaceOfPublication", DbType.String, book.PlaceOfPublication));
                AddParameter(command, CreateParameter("@ISBN", DbType.String, book.ISBN));
                var tableValueParameter = command.Parameters.AddWithValue("@AuthorsIds", dtListOfAuthors);
                tableValueParameter.SqlDbType = SqlDbType.Structured;
                connection.Open();
                bookId = (int)command.ExecuteScalar();
                Console.WriteLine(bookId);
            }
            return bookId;
        }

        public bool DeleteBook(int bookId)
        {
            var result = 0;
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "DeleteBookById");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, bookId));
                connection.Open();
                result = command.ExecuteNonQuery();
            }
            if (result == -1)
            {
                return false;
            }
            return true;
        }

        public IEnumerable<Book> GetAllBooks()
        {
            List<Book> books = new List<Book>();
            List<Author> authors = new List<Author>();
            List<BooksByAuthors> booksByAuthors = new List<BooksByAuthors>();

            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetAllBooks");
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = (int)reader["Id"];
                        var name = reader["Name"].ToString();
                        var publicationYear = (int)reader["PublicationYear"];
                        var countOfPages = (int)reader["CountOfPages"];
                        var note = reader["Note"].ToString();
                        var publishingHouse = reader["PublishingHouse"].ToString();
                        var placeOfPublication = reader["PlaceOfPublication"].ToString();
                        var iSBN = reader["ISBN"].ToString();
                        Book book = new Book(id, name, publicationYear, countOfPages, note, placeOfPublication, publishingHouse, authors, iSBN);
                        books.Add(book);
                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        var authorId = (int)reader["IdAuthor"];
                        var bookId = (int)reader["IdBook"];
                        BooksByAuthors booksByAuthorsItem = new BooksByAuthors(bookId, authorId);
                        booksByAuthors.Add(booksByAuthorsItem);
                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        var id = (int)reader["authorId"];
                        var name = reader["authorName"].ToString();
                        var surname = reader["authorSurname"].ToString();
                        Author author = new Author(id, name, surname);
                        authors.Add(author);
                    }
                }
            }
            foreach (var book in books)
            {
                var booksByAuthorsInfos = booksByAuthors.Where(p => p.BookId == book.Id);
                foreach (var booksByAuthorsInfo in booksByAuthorsInfos)
                {
                    var author = authors.First(p => p.Id == booksByAuthorsInfo.AuthorId);
                    Author newAuthor = new Author(author.Id, author.Name, author.Surname);
                    book.Authors.Add(newAuthor);
                }
            }

            return books;
        }

        public IEnumerable<Author> GetAuthorsByBookId(int bookId)
        {
            List<Author> authors = new List<Author>();
            Author author = null;
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetAuthorsByBookId");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, bookId));
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = (int)reader["Id"];
                        var name = reader["Name"].ToString();
                        var surname = reader["Surname"].ToString();
                        author = new Author(id, name, surname);
                        authors.Add(author);
                    }
                }
            }
            return authors;
        }

        public Book GetBookById(int bookId)
        {
            Book book = null;
            List<Author> authors = new List<Author>();
            List<BooksByAuthors> booksByAuthors = new List<BooksByAuthors>();

            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetBookById");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, bookId));
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = (int)reader["Id"];
                        var name = reader["Name"].ToString();
                        var publicationYear = (int)reader["PublicationYear"];
                        var countOfPages = (int)reader["CountOfPages"];
                        var note = reader["Note"].ToString();
                        var publishingHouse = reader["PublishingHouse"].ToString();
                        var placeOfPublication = reader["PlaceOfPublication"].ToString();
                        var iSBN = reader["ISBN"].ToString();
                        book = new Book(id, name, publicationYear, countOfPages, note, placeOfPublication, publishingHouse, authors, iSBN);
                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        var authorId = (int)reader["IdAuthor"];
                        var idBook = (int)reader["IdBook"];
                        BooksByAuthors booksByAuthorsItem = new BooksByAuthors(idBook, authorId);
                        booksByAuthors.Add(booksByAuthorsItem);
                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        var id = (int)reader["authorId"];
                        var name = reader["authorName"].ToString();
                        var surname = reader["authorSurname"].ToString();
                        Author author = new Author(id, name, surname);
                        authors.Add(author);
                    }
                }
                var booksByAuthorsInfos = booksByAuthors.Where(p => p.BookId == bookId);
                foreach (var booksByAuthorsInfo in booksByAuthorsInfos)
                {
                    var author = authors.First(p => p.Id == booksByAuthorsInfo.AuthorId);
                    Author newAuthor = new Author(author.Id, author.Name, author.Surname);
                    book.Authors.Add(newAuthor);
                }
                
            }
            return book;
        }

        public IEnumerable<Book> GetBooksByAuthorId(int authorId)
        {
            var books = new List<Book>();
            List<Author> authors = new List<Author>();
            Author author = null;

            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetBooksByAuthorId");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, authorId));
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = (int)reader["Id"];
                        var name = reader["Name"].ToString();
                        var publicationYear = (int)reader["PublicationYear"];
                        var countOfPages = (int)reader["CountOfPages"];
                        var note = reader["Note"].ToString();
                        var publishingHouse = reader["PublishingHouse"].ToString();
                        var placeOfPublication = reader["PlaceOfPublication"].ToString();
                        var iSBN = reader["ISBN"].ToString();
                        Book book = new Book(id, name, publicationYear, countOfPages, note, placeOfPublication, publishingHouse, authors, iSBN);
                        books.Add(book);
                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        var id = (int)reader["authorId"];
                        var name = reader["authorName"].ToString();
                        var surname = reader["authorSurname"].ToString();
                        author = new Author(id, name, surname);
                    }
                }
                foreach (var book in books)
                {
                    book.Authors.Add(author);
                   
                }
            }
            return books;
        }

        public void UpdateBook(Book book)
        {
            var bookId = 0;
            DataTable dtListOfAuthors = CreateDtListOfAuthors();
            foreach (Author author in book.Authors)
            {
                DataRow row = dtListOfAuthors.NewRow();
                row["AuthorId"] = author.Id;
                dtListOfAuthors.Rows.Add(row);
            }
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "UpdateBook");
                AddParameter(command, CreateParameter("@Name", DbType.String, book.Name));
                AddParameter(command, CreateParameter("@PublicationYear", DbType.Int32, book.PublicationYear));
                AddParameter(command, CreateParameter("@CountOfPages", DbType.Int32, book.CountOfPages));
                AddParameter(command, CreateParameter("@Note", DbType.String, book.Note));
                AddParameter(command, CreateParameter("@PublishingHouse", DbType.String, book.PublishingHouse));
                AddParameter(command, CreateParameter("@PlaceOfPublication", DbType.String, book.PlaceOfPublication));
                AddParameter(command, CreateParameter("@ISBN", DbType.String, book.ISBN));
                var tableValueParameter = command.Parameters.AddWithValue("@AuthorsIds", dtListOfAuthors);//check no empty
                tableValueParameter.SqlDbType = SqlDbType.Structured;
                connection.Open();
                bookId = (int)command.ExecuteScalar();
                //Console.WriteLine(bookId);
            }
        }
    }
}
