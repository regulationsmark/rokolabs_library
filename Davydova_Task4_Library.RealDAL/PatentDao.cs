﻿using Davydova_Task4_Library.DAL.Interfaces;
using Davydova_Task4_Library.DAL_Interfaces;
using Davydova_Task4_Library.Entities.LinkedDto;
using Davydova_Task4_Library.Library.Documents;
using Davydova_Task4_Library.Library.OtherEntities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.RealDAL
{
    public class PatentDao : SqlDbCommandHelper, IPatentDao
    {
        private IConfigurationRoot _configuration;
        private IAuthorDao _authorDao;
        public PatentDao(IConfigurationRoot configuration, IAuthorDao authorDao)
        {
            _authorDao = authorDao;
            _configuration = configuration;
        }

        private static DataTable CreateDtListOfAuthors()
        {
            var dtListOfAuthors = new DataTable("ListOfAuthors");
            dtListOfAuthors.Columns.Add("AuthorId", typeof(int));
            return dtListOfAuthors;
        }
        public int AddPatent(Patent patent)
        {
            var patentId = 0;
            DataTable dtListOfAuthors = CreateDtListOfAuthors();
            foreach (Author inventor in patent.Inventors)
            {
                DataRow row = dtListOfAuthors.NewRow();
                row["AuthorId"] = inventor.Id;
                dtListOfAuthors.Rows.Add(row);
            }
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "AddPatent");
                AddParameter(command, CreateParameter("@Name", DbType.String, patent.Name));
                AddParameter(command, CreateParameter("@PublicationYear", DbType.Int32, patent.PublicationYear));
                AddParameter(command, CreateParameter("@CountOfPages", DbType.Int32, patent.CountOfPages));
                AddParameter(command, CreateParameter("@Note", DbType.String, patent.Note));
                AddParameter(command, CreateParameter("@RegistrationNumber", DbType.Int32, patent.RegistrationNumber));
                AddParameter(command, CreateParameter("@ApplicationDate", DbType.DateTime, patent.ApplicationDate));
                AddParameter(command, CreateParameter("@PublicationDate", DbType.DateTime, patent.ApplicationDate));
                var tableValueParameter = command.Parameters.AddWithValue("@AuthorsIds", dtListOfAuthors);//check no empty
                tableValueParameter.SqlDbType = SqlDbType.Structured;
                connection.Open();
                patentId = (int)command.ExecuteScalar();
                //Console.WriteLine(patentId);
            }
            return patentId;
        }

        public bool DeletePatent(int patentId)
        {
            var result = 0;
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "DeletePatentByID");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, patentId));
                connection.Open();
                result = command.ExecuteNonQuery();
            }
            if (result == -1)
            {
                return false;
            }
            return true;
        }

        public IEnumerable<Patent> GetAllPatents()
        {
            List<Patent> patents = new List<Patent>();
            List<Author> inventors = new List<Author>();
            List<PatentsByInventors> patentsByInventors = new List<PatentsByInventors>();

            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetAllPatents");
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = (int)reader["Id"];
                        var name = reader["Name"].ToString();
                        var publicationYear = (int)reader["PublicationYear"];
                        var countOfPages = (int)reader["CountOfPages"];
                        var note = reader["Note"].ToString();
                        var country = reader["Country"].ToString();
                        var registrationNumber = (int)reader["RegistrationNumber"];
                        var applicationDate = (DateTime)reader["ApplicationDate"];
                        var publicationDate = (DateTime)reader["PublicationDate"];
                        Patent patent = new Patent(id, name, countOfPages, note, inventors, registrationNumber, applicationDate, publicationDate, country);
                        patents.Add(patent);
                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        var inventorId = (int)reader["IDInventor"];
                        var patentId = (int)reader["IdPatent"];
                        PatentsByInventors patentsByInventorsItem = new PatentsByInventors(patentId, inventorId);
                        patentsByInventors.Add(patentsByInventorsItem);
                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        var id = (int)reader["authorId"];
                        var name = reader["authorName"].ToString();
                        var surname = reader["authorSurname"].ToString();
                        Author inventor = new Author(id, name, surname);
                        inventors.Add(inventor);
                    }
                }
            }
            foreach (var patent in patents)
            {
                var patentsByInventorsInfos = patentsByInventors.Where(p => p.PatentId == patent.Id);
                foreach (var patentsByAuthorsInfo in patentsByInventorsInfos)
                {
                    var inventor = inventors.First(p => p.Id == patentsByAuthorsInfo.InventorId);
                    Author newInventor = new Author(inventor.Id, inventor.Name, inventor.Surname);
                    patent.Inventors.Add(newInventor);
                }
            }

            return patents;
        }

        public Patent GetPatentById(int idPatent)
        {
            Patent book = null;
            List<Author> inventors = new List<Author>();
            List<PatentsByInventors> patentsByInventorsList = new List<PatentsByInventors>();

            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetPatentById");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, idPatent));
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = (int)reader["Id"];
                        var name = reader["Name"].ToString();
                        var publicationYear = (int)reader["PublicationYear"];
                        var countOfPages = (int)reader["CountOfPages"];
                        var note = reader["Note"].ToString();
                        var country = reader["Country"].ToString();
                        var registrationNumber = (int)reader["RegistrationNumber"];
                        var applicationDate = (DateTime)reader["ApplicationDate"];
                        var publicationDate = (DateTime)reader["PublicationDate"];
                        Patent patent = new Patent(id, name, countOfPages, note, inventors, registrationNumber, applicationDate, publicationDate, country);
                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        var inventorId = (int)reader["IDInventor"];
                        var patentId = (int)reader["IdPatent"];
                        PatentsByInventors patentsByInventors = new PatentsByInventors(patentId, inventorId);
                        patentsByInventorsList.Add(patentsByInventors);
                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        var id = (int)reader["authorId"];
                        var name = reader["authorName"].ToString();
                        var surname = reader["authorSurname"].ToString();
                        Author inventor = new Author(id, name, surname);
                        inventors.Add(inventor);
                    }
                }
                var patentsByInventorsInfos = patentsByInventorsList.Where(p => p.PatentId == idPatent);
                foreach (var patentsByInventorsInfo in patentsByInventorsInfos)
                {
                    var inventor = inventors.First(p => p.Id == patentsByInventorsInfo.InventorId);
                    Author newInventor = new Author(inventor.Id, inventor.Name, inventor.Surname);
                    book.Inventors.Add(newInventor);
                }

            }
            return book;
        }

        public IEnumerable<Patent> GetPatentsByInventorId(int inventorId)
        {
            var patents = new List<Patent>();
            List<Author> inventors = new List<Author>();
            Author inventor = null;

            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "GetPatentsByInventorId");
                AddParameter(command, CreateParameter("@Id", DbType.Int32, inventorId));
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var id = (int)reader["Id"];
                        var name = reader["Name"].ToString();
                        var publicationYear = (int)reader["PublicationYear"];
                        var countOfPages = (int)reader["CountOfPages"];
                        var note = reader["Note"].ToString();
                        var country = reader["Country"].ToString();
                        var registrationNumber = (int)reader["RegistrationNumber"];
                        var applicationDate = (DateTime)reader["ApplicationDate"];
                        var publicationDate = (DateTime)reader["PublicationDate"];
                        Patent patent = new Patent(id, name, countOfPages, note, inventors, registrationNumber, applicationDate, publicationDate, country);
                        patents.Add(patent);
                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        var id = (int)reader["authorId"];
                        var name = reader["authorName"].ToString();
                        var surname = reader["authorSurname"].ToString();
                        inventor = new Author(id, name, surname);
                    }
                }
                foreach (var patent in patents)
                {
                    patent.Inventors.Add(inventor);

                }
            }
            return patents;
        }

        public void UpdatePatent(Patent patent)
        {
            var patentId = 0;
            DataTable dtListOfAuthors = CreateDtListOfAuthors();
            foreach (Author author in patent.Inventors)
            {
                DataRow row = dtListOfAuthors.NewRow();
                row["AuthorId"] = author.Id;
                dtListOfAuthors.Rows.Add(row);
            }
            using (var connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                var command = CreateCommand(connection, "UpdatePatent");
                AddParameter(command, CreateParameter("@Name", DbType.String, patent.Name));
                AddParameter(command, CreateParameter("@PublicationYear", DbType.Int32, patent.PublicationYear));
                AddParameter(command, CreateParameter("@CountOfPages", DbType.Int32, patent.CountOfPages));
                AddParameter(command, CreateParameter("@Note", DbType.String, patent.Note));
                AddParameter(command, CreateParameter("@RegistrationNumber", DbType.Int32, patent.RegistrationNumber));
                AddParameter(command, CreateParameter("@ApplicationDate", DbType.DateTime, patent.ApplicationDate));
                AddParameter(command, CreateParameter("@PublicationDate", DbType.DateTime, patent.ApplicationDate));
                var tableValueParameter = command.Parameters.AddWithValue("@AuthorsIds", dtListOfAuthors);//check no empty
                tableValueParameter.SqlDbType = SqlDbType.Structured;
                connection.Open();
                patentId = (int)command.ExecuteScalar();
                //Console.WriteLine(bookId);
            }
        }
    }
}
